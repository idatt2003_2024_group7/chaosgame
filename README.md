# Chaos Game

The ChaosGame generates digital fractals using the Chaos Game algorithm.

## Description

The Chaos Game is a mathematical game. When creating a Chaos Game the user starts by creating a list of transformations,
either Julia or Affine. The transformations are then applied to a starting point and used to calculate new points.
This process is repeated a number of times, creating a fractal.

This application supports the following features:

* Create new fractals using either Affine or Julia transformations
* Choose predefined fractals
* Visualize the fractals
* Save fractals as a .txt, .png or .jpeg file
* Load fractals from a .txt file
* Edit chosen fractals and number of iterations
* Canvas settings such as coloured rendering and detailed render for Julia transformations

### Features
#### Creating new fractals
This feature allows the user to create new fractals using either Affine or Julia transformations.
When running the program choose the "New Fractal" >> "New Fractal" in the menu bar. This will then let the user 
choose between creating Affine or Julia transformations.
When choosing Affine, the user can add additional transformations by clicking the "+ Row" button.
Before pressing the "Continue" button make sure all input is filled in correctly, both the transformation/transformations
and the min and max values for the x and y coordinates.

#### Choosing predifined fractals
This feature allows the user to choose predifined fractals.
When running the program choose the "New fractal" >> "Predifined Fractals" in the menu bar.

#### Visualizing the fractals
This feature allows the user to visualize the fractals.
This will autmaticly be done when creating a new fractal, 
loading a fractal or pressing the "Run" button when editing a fractal.

#### Saving fractals
This feature allows the user to save fractals as a .txt, .png or .jpeg file.
When running the program choose the "Save fractal" in the menu bar.
The user can then choose the "Save to path", "Save to folder" or "Save as image" option.
Save to path will save the current fractal as a .txt file in the chosen path.
Save to folder will save the current fractal as a .txt file in the fractals folder ing the project.
Save as image will save the current fractal as a .png or .jpeg file in the chosen path.

#### Loading fractals
This feature allows the user to load fractals from a .txt file.
When running the program choose the "Load fractal" in the menu bar and then "Load from path" or "Load from folder".
Loading from path will let the user choose a .txt file to load.
Loading from folder will let the user choose a .txt file from the fractals folder in the project.

#### Editing chosen fractals and number of iterations
This feature allows the user to edit chosen fractals and number of iterations.
When running the program choose the "Edit Fractal" button in the menu bar.
This will open a window where the user can edit the chosen fractal and number of iterations.
To run the fractal press the "Run" button.
To close the window press the "Edit Fractal" button again.

Note: the maximum number of iterations is set to 10e8. Input higher than this will not be accepted. 
A higher number of iterations might cause a long loading time.

#### Coloured rendering of the fractals
This feature allows the user to choose between a coloured or black and white rendering of the fractals.
When running the program choose the "Settings" option in the menu bar.
This will let the user toggle the following settings:
* Coloured rendering
* Change canvas colour
* Detailed render for Julia transformations

Coloured rendering will render the fractals in colour. 
The colour of the fractals is based how many points have the same amount of iterations.
Changing the canvas colour will turn the canvas black, and the pen coulor white, this can also be used with the coloured rendering.
The detailed render for Julia transformations will render the fractals in more detail, for best results use with coloured rendering.

Note: the detailed render for Julia transformations might cause a long loading time, especially with a high number of iterations.
### Project structure

list packages and files, including a brief description of each and links.

All source files are stored in [src](./src). 
* The [main/java/edu.ntnu.idatt2003.group7.chaosgame](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame) package contains the main program structure: 
  * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.controller](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/controller) package contains the controller classes.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.controller.cli](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/controller/cli) package contains the CLI class.
  * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model) package contains the model classes.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model.factory](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model/factory) package contains the factory class.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model.fractal](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model/fractal) package contains the fractal classes.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model.io](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model/io) package contains the utility class.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model.math](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model/math) package contains the maths classes.
    * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.model.transformations](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/model/transformations) package contains the transformation classes.
  * The [main/java/edu.ntnu.idatt2003.group7.chaosgame.view](./src/main/java/edu/ntnu/idatt2003/group7/chaosgame/view) package contains the view classes.
  * The [test/java/edu.ntnu.idatt2003.group7.chaosgame](./src/test/java/edu/ntnu/idatt2003/group7/chaosgame) package contains the unit tests.

### Repository

[Chaos Game](https://gitlab.stud.idi.ntnu.no/idatt2003_2024_group7/chaosgame)

## Getting Started

### Dependencies
* JDK 21
* Maven
* Javafx
* JUnit
* SLF4J


### Installing

To install the program, you need to clone or download the project repository from [Chaos Game](https://gitlab.stud.idi.ntnu.no/idatt2003_2024_group7/chaosgame).
Make sure JDK 21 is installed.
Use Apache Maven or an IDE that supports Maven to run the program.
### Executing program

Using an IDE:
* Open the project in your IDE
* Run the following command in terminal:
```
mvn javafx:run
```

Using Apache Maven:
* Open the terminal and navigate to project folder
* Run the following command:
```
mvn javafx:run
```

## Authors

Helene Kjerstad, Ylva Björnerstedt

## Version History
* 1.0
    * Final version
    * Fixed bugs and cleaned up code
* 0.3
    * Implemented GUI
    * Added additional features:
      * Save fractals as a .txt, .png or .jpeg file
      * Colour rendering of the fractals
      * Dark mode for canvas
      * Detailed render for Julia transformations
* 0.2
    * Added Chaos Game algorithm
    * Added CLI
    * Added reading and writing to file
* 0.1
    * Implementation of transformations, vectors and matrices

## Acknowledgments

AI used in this project:
* GitHub Copilot
* ChatGPT
