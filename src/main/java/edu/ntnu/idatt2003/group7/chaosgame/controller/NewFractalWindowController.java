package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import edu.ntnu.idatt2003.group7.chaosgame.view.components.NewFractalWindow;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The NewFractalWindowController class is the controller for the NewFractalWindow.
 * <p>
 * The class provides methods to connect the logic and the view of the NewFractalWindow
 * which is used to create new fractals.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class NewFractalWindowController {
  /**
   * The NewFractalWindow object.
   */
  private final NewFractalWindow newFractalWindow;
  /**
   * The consumer that loads the chaos game.
   */
  private final Consumer<ChaosGameDescription> loadChaosGame;
  /**
   * The logger object.
   */
  private static final Logger logger = LoggerFactory.getLogger(NewFractalWindowController.class);

  /**
   * Constructor for NewFractalWindowController,
   * creates a new controller for the NewFractalWindow and sets up the window.
   *
   * @param newFractalWindow The NewFractalWindow object.
   * @param loadChaosGame The consumer that loads the chaos game.
   */
  public NewFractalWindowController(NewFractalWindow newFractalWindow,
                                    Consumer<ChaosGameDescription> loadChaosGame) {
    this.newFractalWindow = newFractalWindow;
    this.loadChaosGame = loadChaosGame;
    setup();
  }

  /**
   * Sets up the NewFractalWindow.
   * <p>
   *   The method sets up the NewFractalWindow by setting the close button,
   *   the add row button, the save affine button, the save julia button,
   *   the affine table, and the close window button.
   * </p>
   */
  private void setup() {
    setCloseButton();
    setAddRowButton();
    setSaveAffine();
    setSaveJulia();
    setAffineTable();
    closeWindow();
  }

  /**
   * Sets the close button.
   */
  private void setCloseButton() {
    newFractalWindow.getCloseButton().setOnAction(e -> closeWindow());
  }

  /**
   * Sets the add row button.
   */
  private void setAddRowButton() {
    newFractalWindow.getAddRow().setOnAction(event -> addAffineRow());
  }

  /**
   * Sets the save julia button.
   */
  private void setSaveJulia() {
    newFractalWindow.getSaveJulia().setOnAction(event -> makeJulia());
  }

  /**
   * Sets the save affine button.
   */
  private void setSaveAffine() {
    newFractalWindow.getSaveAffine().setOnAction(event -> makeAffine());
  }

  /**
   * Makes a Julia fractal.
   *
   * <p>The method makes a Julia fractal by reading the text fields in the NewFractalWindow,
   *   parsing the values, and creating a list of transformations.
   *   If an error occurs when reading the text fields, the transforms list will be empty.
   *   The method then creates a ChaosGameDescription object and loads the chaos game.
   *   The method also clears the text fields in the NewFractalWindow. </p>
   *
   */
  private void makeJulia() {
    newFractalWindow.getNotificationBox().clear();
    newFractalWindow.removeNotification();
    List<Transform2D> transforms = new ArrayList<>();
    double real = 0;
    double imaginary = 0;
    int count = 0;
    List<List<TextField>> textFields = newFractalWindow.getJuliaTextFields();

    // Read all text fields
    try {
      for (List<TextField> row : textFields) {
        for (TextField textField : row) {
          if (count == 0) {
            real = Utility.parseDoubleTextField(textField);
            count++;
          } else {
            imaginary = Utility.parseDoubleTextField(textField);
            count = 0;
          }

          // Add new julia transforms
          Complex point = new Complex(real, imaginary);
          transforms.add(new JuliaTransform(point, 1));
          transforms.add(new JuliaTransform(point, -1));
        }
      }
    } catch (NumberFormatException e) {
      setNotification("Invalid input for transformation: Please enter a number");
    } catch (IndexOutOfBoundsException e) {
      setNotification("Invalid input for transformation: " + e.getMessage());
      transforms = new ArrayList<>();
    } catch (Exception e) {
      logger.atError().log("Error occurred when reading Julia textFields: " + e.getMessage());
      transforms = new ArrayList<>();
    }

    // Read min and max values
    Vector2D min = null;
    Vector2D max = null;
    try {
      min = Utility.parseVector(newFractalWindow.getJuliaMinField().getText());
      max = Utility.parseVector(newFractalWindow.getJuliaMaxField().getText());
    } catch (Exception e) {
      setNotification("Invalid input for min/max values: " + e.getMessage());
    }

    // Make game description
    try {
      ChaosGameDescription chaosGameDescription =
          new ChaosGameDescription(transforms, min, max);
      loadChaosGame.accept(chaosGameDescription);
      clearTextFields();
      closeWindow();
    } catch (Exception e) {
      logger.atError().log("Error occurred when creating ChaosGameDescription: " + e.getMessage());
    }
  }

  /**
   * Makes an affine fractal.
   *
   * <p>The method makes an affine fractal by reading the text fields in the NewFractalWindow,
   *   parsing the values, and creating a list of transformations.
   *   If an error occurs when reading the text fields, the transforms list will be empty.
   *   The method then creates a ChaosGameDescription object and loads the chaos game.
   *   The method also clears the text fields in the NewFractalWindow.
   *
   */
  private void makeAffine() {
    newFractalWindow.getNotificationBox().clear();
    newFractalWindow.removeNotification();
    int count = 0;
    Matrix2x2 matrix = null;
    Vector2D vector = null;
    List<Transform2D> transforms = new ArrayList<>();
    List<List<TextField>> textFields = newFractalWindow.getAffineTextFields();
    try {
      for (List<TextField> row : textFields) {
        for (TextField textField : row) {
          if (count == 0) {
            // Matrix
            matrix = Utility.parseMatrix(textField.getText());
            count++;
          } else {
            // x and y values
            vector = Utility.parseVector(textField.getText());
            count = 0;
          }
        }
        transforms.add(new AffineTransform2D(matrix, vector));
      }
    } catch (NumberFormatException e) {
      setNotification("Invalid input for transformation: Please enter a number");
      transforms = new ArrayList<>();
    } catch (IllegalArgumentException e) {
      setNotification(e.getMessage());
      transforms = new ArrayList<>();
    } catch (Exception e) {
      logger.atError().log("Error occurred when reading Affine textFields: " + e.getMessage());
      transforms = new ArrayList<>();
    }

    // Read min and max values
    Vector2D min = null;
    Vector2D max = null;
    try {
      min = Utility.parseVector(newFractalWindow.getAffineMinField().getText());
      max = Utility.parseVector(newFractalWindow.getAffineMaxField().getText());
    } catch (Exception e) {
      setNotification("Invalid input for min/max values: " + e.getMessage());
    }

    // Make description and load game
    try {
      ChaosGameDescription chaosGameDescription =
          new ChaosGameDescription(transforms, min, max);
      loadChaosGame.accept(chaosGameDescription);

      // Clear the affine table and text fields, and set up again
      newFractalWindow.getAffineTextFields().clear();
      newFractalWindow.getAffineTable().getChildren().clear();
      newFractalWindow.affineTableHeader();
      newFractalWindow.addAffineRow(newFractalWindow.getRowCount());
      clearTextFields();
      closeWindow();
    } catch (Exception e) {
      logger.atError().log("Error occurred when creating ChaosGameDescription: " + e.getMessage());
    }
  }

  /**
   * Sets a notification in the NewFractalWindow.
   *
   * @param message The message to be displayed.
   */
  private void setNotification(String message) {
    if (newFractalWindow.getNotificationBox().isEmpty()) {
      newFractalWindow.addNotification();
    }
    newFractalWindow.getNotificationBox().add(message);
  }

  /**
   * Clears the text fields in the NewFractalWindow.
   */
  private void clearTextFields() {
    for (List<TextField> row : newFractalWindow.getAffineTextFields()) {
      for (TextField textField : row) {
        textField.clear();
      }
    }
    for (List<TextField> row : newFractalWindow.getJuliaTextFields()) {
      for (TextField textField : row) {
        textField.clear();
      }
    }
  }

  /**
   * Closes the NewFractalWindow.
   */
  private void closeWindow() {
    newFractalWindow.setVisible(false);
  }

  /**
   * Sets the affine table with one row of text fields.
   */
  private void setAffineTable() {
    newFractalWindow.addAffineRow(newFractalWindow.getRowCount());
  }

  /**
   * Adds a row to the affine table.
   */
  private void addAffineRow() {
    int row = newFractalWindow.getRowCount();

    // Add button to remove row
    Button remove = new Button("X");
    remove.setOnAction(event ->
        newFractalWindow.getAffineTable().getChildren()
            .removeIf(node -> GridPane.getRowIndex(node) == row));
    remove.getStyleClass().add("remove-button");
    // Add row to table
    newFractalWindow.addRemoveAffineRow(row, remove);
  }

  /**
   * Displays the NewFractalWindow.
   */
  public void display() {
    newFractalWindow.setVisible(true);
  }

}
