package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 * The Page class for the Chaos Game application.
 * <p>
 * This class is a custom {@link StackPane} that contains the
 * CanvasView, TopBar, NewFractalWindow, FractalWindow and exit button of the Page.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see StackPane
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class Page extends StackPane {
  /**
   * The CanvasView of the Page.
   */
  private CanvasView canvasView;
  /**
   * The TopBar of the Page.
   */
  private TopBar topBar;
  /**
   * The NewFractalWindow of the Page.
   */
  private NewFractalWindow newFractalWindow;
  /**
   * The FractalWindow of the Page.
   */
  private FractalWindow fractalWindow;
  /**
   * The exit button of the Page.
   */
  private Button exitButton;

  /**
   * The constructor for the Page.
   */
  public Page() {
    make();
  }

  /**
   * Returns the CanvasView of the Page.
   *
   * @return the CanvasView of the Page.
   */
  public CanvasView getCanvas() {
    return canvasView;
  }

  /**
   * Returns the TopBar of the Page.
   *
   * @return the TopBar of the Page.
   */
  public TopBar getTopBar() {
    return topBar;
  }

  /**
   * Returns the NewFractalWindow of the Page.
   *
   * @return the NewFractalWindow of the Page.
   */
  public NewFractalWindow getNewFractalWindow() {
    return newFractalWindow;
  }

  /**
   * Returns the FractalWindow of the Page.
   *
   * @return the FractalWindow of the Page.
   */
  public FractalWindow getFractalWindow() {
    return fractalWindow;
  }

  /**
   * Returns the exit button of the Page.
   *
   * @return the exit button of the Page.
   */
  public Button getExitButton() {
    return exitButton;
  }

  /**
   * Creates the Page.
   */
  private void make() {
    canvasView = new CanvasView(800, 600);
    topBar = new TopBar();
    newFractalWindow = new NewFractalWindow();
    fractalWindow = new FractalWindow();

    exitButton = new Button("Exit");
    exitButton.getStyleClass().add("border-pane-button");

    BorderPane borderPane = new BorderPane();
    borderPane.getStyleClass().add("border-pane");
    borderPane.setTop(topBar);
    borderPane.setRight(fractalWindow);
    borderPane.setBottom(exitButton);

    this.getChildren().addAll(canvasView, borderPane, newFractalWindow);
  }
}
