package edu.ntnu.idatt2003.group7.chaosgame.view;

import edu.ntnu.idatt2003.group7.chaosgame.controller.CanvasController;
import edu.ntnu.idatt2003.group7.chaosgame.controller.ChaosGameController;
import edu.ntnu.idatt2003.group7.chaosgame.controller.FractalWindowController;
import edu.ntnu.idatt2003.group7.chaosgame.controller.NewFractalWindowController;
import edu.ntnu.idatt2003.group7.chaosgame.controller.TopBarController;
import edu.ntnu.idatt2003.group7.chaosgame.view.components.Page;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * The App class for the Chaos Game application.
 * <p>
 * This class is the main class for the JavaFX application. It sets up the stage and scene for the
 * application and initializes the controllers for the different components of the application.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class App extends Application {
  private ChaosGameController chaosGameController;

  /**
   * The main method for the Chaos Game application.
   *
   * @param args the command-line arguments.
   */
  public static void main(String[] args) {
    Application.launch(args);
  }

  /**
   * The start method for the Chaos Game application.
   *
   * @param stage the stage to display the application.
   */
  @Override
  public void start(Stage stage) {
    Page mainPage = new Page();
    Scene scene = new Scene(mainPage, 1000, 650);
    chaosGameController =
        new ChaosGameController((int) scene.getWidth(), (int) scene.getHeight());
    TopBarController topBarController =
        new TopBarController(mainPage.getTopBar(), chaosGameController::loadChaosGame);
    CanvasController canvasController = new CanvasController(mainPage.getCanvas());
    NewFractalWindowController newFractalWindowController = new NewFractalWindowController(
        mainPage.getNewFractalWindow(), chaosGameController::loadChaosGame);
    FractalWindowController fractalWindowController = new FractalWindowController(
        mainPage.getFractalWindow(), chaosGameController::updateChaosGame,
        chaosGameController::runChaosGame, topBarController::saveToFileChooser);
    fractalWindowController.setFractalButton(mainPage.getTopBar().getFractalButton());


    // initialize listeners
    mainPage.getTopBar().getMenuBar().getNewFractalMenuItem().setOnAction(
        e -> newFractalWindowController.display());
    mainPage.getTopBar().getMenuBar().getSaveToPathMenuItem().setOnAction(e ->
        topBarController.saveToFileChooser(chaosGameController.getChaosGame().getDescription()));
    mainPage.getTopBar().getMenuBar().getSaveToFolderMenuItem().setOnAction(
        e -> topBarController.saveToFolder(chaosGameController.getChaosGame().getDescription()));
    mainPage.getTopBar().getMenuBar().getSaveAsImage().setOnAction(
        e -> topBarController.saveImage(canvasController.getBufferedImage()));
    mainPage.getExitButton().setOnAction(e -> stop());

    chaosGameController.addChaosGameObserver(fractalWindowController);
    chaosGameController.addChaosGameObserver(canvasController);

    canvasController.addSizeObserver(chaosGameController::updateCanvasSize);

    topBarController.addRenderColorObserver(canvasController::setColoredCanvas);
    topBarController.addDarkCanvasObserver(canvasController::setDarkCanvas);
    topBarController.addDetailedRenderObserver(chaosGameController::setDetailedRender);

    mainPage.getTopBar().getMenuBar().prefWidthProperty().bind(scene.widthProperty());

    // Stage settings
    scene.getStylesheets().add("file:src/main/resources/styles.css");
    stage.setTitle("Chaos Game");

    // Set the stage to full screen
    Screen screen = Screen.getPrimary();
    stage.setX(screen.getVisualBounds().getMinX());
    stage.setY(screen.getVisualBounds().getMinY());
    stage.setMaximized(true);

    stage.setMinWidth(1000);
    stage.setMinHeight(650);

    stage.setScene(scene);
    stage.show();
    stage.setFullScreen(true);
  }

  @Override
  public void stop() {
    chaosGameController.cleanUp();
    System.exit(0);
  }
}
