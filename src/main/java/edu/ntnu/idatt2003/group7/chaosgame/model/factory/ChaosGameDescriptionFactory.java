package edu.ntnu.idatt2003.group7.chaosgame.model.factory;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Factory class for creating ChaosGameDescription objects.
 * <p>
 * Can be used to make presets of the julia transform, sierpinski affine and barnsley affine.
 * Can also create a ChaosGameDescription object for a julia transform wih a given complex constant.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see ChaosGameDescription
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGameDescriptionFactory {

  /**
   * Private constructor to prevent instantiation of the class.
   */
  private ChaosGameDescriptionFactory() {}

  /**
   * Creates a ChaosGameDescription object for the julia transform
   * with the complex constant -0.74543 + 0.11301i.
   *
   * @return a ChaosGameDescription object for the julia transform
   */
  public static ChaosGameDescription juliaDescription() {
    Complex c = new Complex(-.74543, .11301);
    return juliaDescription(c);
  }

  /**
   * Creates a ChaosGameDescription object for the julia transform with the given complex constant.
   *
   * @param c the complex constant
   * @return a ChaosGameDescription object for the julia transform
   */
  public static ChaosGameDescription juliaDescription(Complex c) {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new JuliaTransform(c, 1));
    transforms.add(new JuliaTransform(c, -1));

    return new ChaosGameDescription(transforms, new Vector2D(-1.6, -1), new Vector2D(1.6, 1));
  }

  /**
   * Creates a ChaosGameDescription object for the sierpinski affine.
   * Using the following transformations: <ul>
   * <li>1. Matrix((.5, 0),(0,.5)) and vector (0, 0)</li>
   * <li>2. Matrix((.5, 0),(0,.5)) and vector (0.25, 0.5)</li>
   * <li>3. Matrix((.5, 0),(0,.5)) and vector (0.5, 0)</li>
   * </ul>
   *
   * @return a ChaosGameDescription object for the sierpinski affine
   */
  public static ChaosGameDescription sierpinskiDescription() {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(.5, 0, 0, .5), new Vector2D(0, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(.5, 0, 0, .5), new Vector2D(.25, .5)));
    transforms.add(new AffineTransform2D(new Matrix2x2(.5, 0, 0, .5), new Vector2D(.5, 0)));
    return new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(1, 1));
  }

  /**
   * Creates a ChaosGameDescription object for the barnsley affine.
   * Using the following transformations: <ul>
   * <li>1. Matrix((0, 0),(0,.16)) and vector (0, 0)</li>
   * <li>2. Matrix((.85, .04),(-.04,.85)) and vector (0, 1.6)</li>
   * <li>3. Matrix((.2, -.26),(.23,.22)) and vector (0, 1.6)</li>
   * <li>4. Matrix((-.15, .28),(.26,.24)) and vector (0, .44)</li>
   * </ul>
   *
   * @return a ChaosGameDescription object for the barnsley affine
   */
  public static ChaosGameDescription barnsleyDescription() {
    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0, 0, 0, .16), new Vector2D(0, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(.85, .04, -.04, .85), new Vector2D(0, 1.6)));
    transforms.add(new AffineTransform2D(new Matrix2x2(.2, -.26, .23, .22), new Vector2D(0, 1.6)));
    transforms.add(new AffineTransform2D(new Matrix2x2(-.15, .28, .26, .24), new Vector2D(0, .44)));
    return new ChaosGameDescription(transforms, new Vector2D(-2.65, 0), new Vector2D(2.65, 10));
  }
}
