package edu.ntnu.idatt2003.group7.chaosgame.model.fractal;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;

/**
 * The ChaosCanvas class represents a 2D canvas.
 * <p>
 * The class provides methods to set and get pixels on the canvas.
 * </p>
 *
 * @version 1.0
 * @since 0.2
 * @author Helene Kjerstad
 */
public class ChaosCanvas {
  /**
   * The 2D canvas array of the ChaosCanvas object.
   */
  private final int[][] canvas;
  /**
   * The width of the ChaosCanvas object.
   */
  private final int width;
  /**
   * The height of the ChaosCanvas object.
   */
  private final int height;
  /**
   * The minimum coordinates of the ChaosCanvas object.
   */
  private final Vector2D minCoords;
  /**
   * The maximum coordinates of the ChaosCanvas object.
   */
  private final Vector2D maxCoords;
  /**
   * The transformation for transforming coordinates to indices of the ChaosCanvas object.
   */
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Validates that the vector is not null.
   *
   * @param vector The vector.
   * @param name The name of the parameter.
   *
   * @throws IllegalArgumentException if the vector is null.
   */
  private void validateVector(Vector2D vector, String name) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException(String.format("The vector %s cannot be null", name));
    }
  }

  /**
   * Validates that the minCoords are smaller than the maxCoords.
   *
   * @param minCoords The minimum coordinates.
   * @param maxCoords The maximum coordinates.
   *
   * @throws IllegalArgumentException if the minCoords are not smaller than the maxCoords.
   */
  private void validateCoords(Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    if (minCoords.getX1() >= maxCoords.getX1() || minCoords.getX0() >= maxCoords.getX0()) {
      throw new IllegalArgumentException("The minCoords must be smaller than the maxCoords");
    }
  }

  /**
   * Validates that the value is a positive int.
   *
   * @param value The value.
   * @param name The name of the parameter.
   *
   * @throws IllegalArgumentException if the value is not a positive int.
   */
  private void validatePositiveInt(int value, String name) throws IllegalArgumentException {
    if (value <= 0) {
      throw new IllegalArgumentException(String.format("The %s must be a positive int", name));
    }
  }

  /**
   * Transforms the given point to indices.
   *
   * @param point The point to transform.
   *
   * @return the transformed point
   *
   * @throws IllegalArgumentException if the point is null
   */
  private Vector2D transformCoordsToIndices(Vector2D point) throws IllegalArgumentException {
    validateVector(point, "point");
    return transformCoordsToIndices.transform(point);
  }

  /**
   * Constructor for ChaosCanvas,
   * creates a new 2D canvas with the given width, height, minCoords and maxCoords.
   *
   * @param width     The width.
   * @param height    The height.
   * @param minCoords The minimum coordinates.
   * @param maxCoords The maximum coordinates.
   *
   * @throws IllegalArgumentException if: <ul>
   *      <li>width is not a positive int</li>
   *      <li>height is not a positive int</li>
   *      <li>minCoords is null</li>
   *      <li>maxCoords is null</li>
   *      </ul>
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    try {
      validateVector(minCoords, "minCoords");
      validateVector(maxCoords, "maxCoords");
      validateCoords(minCoords, maxCoords);
      validatePositiveInt(width, "width");
      validatePositiveInt(height, "height");

      this.width = width;
      this.height = height;
      this.minCoords = minCoords;
      this.maxCoords = maxCoords;
      this.canvas = new int[height][width];
      this.transformCoordsToIndices = new AffineTransform2D(
          new Matrix2x2(
              0, (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
              (width - 1) / (maxCoords.getX0() - minCoords.getX0()), 0),
          new Vector2D(
              ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
              ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0()))
      );
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create ChaosCanvas object. " + e.getMessage());
    }
  }

  /**
   * Returns the width of the canvas.
   *
   * @return the width of the canvas
   */
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height of the canvas.
   *
   * @return the height of the canvas
   */
  public int getHeight() {
    return height;
  }

  /**
   * Returns the minimum coordinates of the canvas.
   *
   * @return the minimum coordinates of the canvas
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates of the canvas.
   *
   * @return the maximum coordinates of the canvas
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Returns the canvas array.
   *
   * @return the canvas array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Returns the pixel at the given point.
   *
   * @param point the point to get the pixel from
   * @return the pixel at the given point
   * @throws IllegalArgumentException if the point is null
   */
  public int getPixel(Vector2D point) throws IllegalArgumentException {
    Vector2D indices = transformCoordsToIndices(point);
    return canvas[(int) indices.getX0()][(int) indices.getX1()];
  }

  /**
   * Sets the pixel at the given point.
   *
   * @param point the point to set the pixel at
   * @throws IllegalArgumentException if the point is null
   */
  public void setPixel(Vector2D point) throws IllegalArgumentException {
    Vector2D indices = transformCoordsToIndices(point);
    canvas[(int) indices.getX0()][(int) indices.getX1()] += 1;
  }

  /**
   * Checks if the given point is within the canvas.
   *
   * @param point the point to check
   * @return true if the point is within the canvas, false otherwise
   * @throws IllegalArgumentException if the point is null
   */
  public boolean isWithinCanvas(Vector2D point) throws IllegalArgumentException {
    validateVector(point, "point");
    return point.getX0() >= minCoords.getX0() && point.getX0() <= maxCoords.getX0()
        && point.getX1() >= minCoords.getX1() && point.getX1() <= maxCoords.getX1();
  }

  /**
   * Clears the canvas.
   */
  public void clear() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }
}
