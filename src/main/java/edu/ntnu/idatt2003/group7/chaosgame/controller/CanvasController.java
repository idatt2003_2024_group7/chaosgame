package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosCanvas;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import edu.ntnu.idatt2003.group7.chaosgame.view.components.CanvasView;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import javafx.beans.value.ChangeListener;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The CanvasController class is the controller for the canvas.
 * <p>
 * The class provides methods to connect the logic and the view of the canvas.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */

public class CanvasController implements ChaosGameObserver {
  /**
   * The canvasView object.
   */
  private final CanvasView canvasView;
  /**
   * The pen color of the canvas.
   */
  private Color penColor;
  /**
   * The background color of the canvas.
   */
  private Color canvasColor;
  /**
   * The boolean value that determines if the canvas is colored.
   */
  private boolean coloredCanvas;
  /**
   * The BufferedImage object.
   */
  private BufferedImage bufferedImage;
  /**
   * The list of size observers.
   */
  private final List<Consumer<Vector2D>> sizeObservers;

  /**
   * The logger object.
   */
  private static final Logger logger = LoggerFactory.getLogger(CanvasController.class);

  /**
   * Renders the canvas.
   * <p>
   *   The method renders the canvas with the given
   *   ChaosCanvas object using WritableImage and BufferedImage.
   * </p>
   *
   * @param chaosCanvas The ChaosCanvas object.
   */
  private void renderCanvas(ChaosCanvas chaosCanvas) {
    WritableImage writableImage = new WritableImage(
        chaosCanvas.getWidth(), chaosCanvas.getHeight());
    bufferedImage = new BufferedImage(chaosCanvas.getWidth(),
        chaosCanvas.getHeight(), BufferedImage.TYPE_INT_RGB);

    // render canvas
    for (int y = 0; y < chaosCanvas.getHeight(); y++) {
      for (int x = 0; x < chaosCanvas.getWidth(); x++) {
        if (chaosCanvas.getCanvasArray()[y][x] != 0) {
          writableImage.getPixelWriter().setColor(x, y, penColor);
          bufferedImage.setRGB(x, y, Utility.colorToRGB(penColor));
        } else {
          writableImage.getPixelWriter().setColor(x, y, canvasColor);
          bufferedImage.setRGB(x, y, Utility.colorToRGB(canvasColor));
        }
      }
    }
    canvasView.getImageView().setImage(writableImage);
  }

  /**
   * Renders the colored canvas.
   * <p>
   *   The method renders the colored canvas with the given
   *   ChaosCanvas object using WritableImage and BufferedImage.
   * </p>
   *
   * @param chaosCanvas The ChaosCanvas object.
   */
  private void renderColoredCanvas(ChaosCanvas chaosCanvas) {
    WritableImage writableImage = new WritableImage(
        chaosCanvas.getWidth(), chaosCanvas.getHeight());
    bufferedImage = new BufferedImage(chaosCanvas.getWidth(),
        chaosCanvas.getHeight(), BufferedImage.TYPE_INT_RGB);

    // add values to histogram - count occurrences of each value
    Map<Integer, Integer> histogram = new HashMap<>();
    for (int j = 0; j < chaosCanvas.getHeight(); j++) {
      for (int i = 0; i < chaosCanvas.getWidth(); i++) {
        int value = chaosCanvas.getCanvasArray()[j][i];
        if (value != 0 && histogram.containsKey(value)) {
          histogram.put(value, histogram.get(value) + 1);
        } else {
          histogram.put(value, 1);
        }
      }
    }
    // get min and max values from histogram
    int minVal = histogram.entrySet().stream().min(Map.Entry.comparingByValue()).get().getValue();
    int maxVal = histogram.entrySet().stream().max(Map.Entry.comparingByValue()).get().getValue();
    Color color;
    // render canvas
    for (int y = 0; y < chaosCanvas.getHeight(); y++) {
      for (int x = 0; x < chaosCanvas.getWidth(); x++) {
        if (chaosCanvas.getCanvasArray()[y][x] != 0) {
          // get color from value in histogram
          try {
            color = Utility.getColorFromValue(
                histogram.get(chaosCanvas.getCanvasArray()[y][x]), minVal, maxVal);
          } catch (IllegalArgumentException e) {
            logger.error(String.format("Error occurred when rendering canvas: %s", e.getMessage()));
            chaosCanvas.clear();
            return;
          }

          int rgb = Utility.colorToRGB(color);
          writableImage.getPixelWriter().setColor(x, y, color);
          bufferedImage.setRGB(x, y, rgb);
        } else {
          writableImage.getPixelWriter().setColor(x, y, canvasColor);
          bufferedImage.setRGB(x, y, Utility.colorToRGB(canvasColor));
        }
      }
    }
    canvasView.getImageView().setImage(writableImage);
  }

  /**
   * Initializes the observers. Adding a ChangeListener to the width and height of the canvasView.
   */
  private void initializeObservers() {
    ChangeListener<Number> sizeChangeListener = (obs, oldVal, newVal) -> {
      if (canvasView.getWidth() > 0 && canvasView.getHeight() > 0) {
        sizeObservers.forEach(vector2DConsumer ->
            vector2DConsumer.accept(new Vector2D(canvasView.getWidth(), canvasView.getHeight())));
      }
    };
    canvasView.widthProperty().addListener(sizeChangeListener);
    canvasView.heightProperty().addListener(sizeChangeListener);
  }

  /**
   * Constructor for CanvasController,
   * creates a new CanvasController with the given canvasView.
   *
   * @param canvasView The canvasView object.
   */
  public CanvasController(CanvasView canvasView) {
    this.canvasView = canvasView;
    this.penColor = Color.BLACK;
    this.canvasColor = Color.WHITE;
    this.coloredCanvas = false;
    this.sizeObservers = new ArrayList<>();

    initializeObservers();
  }

  /**
   * Determines if the canvas is colored.
   *
   * @param coloredCanvas the boolean value.
   */
  public void setColoredCanvas(boolean coloredCanvas) {
    this.coloredCanvas = coloredCanvas;
  }

  /**
   * Determines if the canvas is dark. This changes the pen color and the canvas color.
   *
   * @param darkCanvas the boolean value.
   */
  public void setDarkCanvas(boolean darkCanvas) {
    if (darkCanvas) {
      canvasColor = Color.BLACK;
      penColor = Color.WHITE;
    } else {
      canvasColor = Color.WHITE;
      penColor = Color.BLACK;
    }
  }

  /**
   * Adds a size observer.
   *
   * @param observer The size observer.
   */
  public void addSizeObserver(Consumer<Vector2D> observer) {
    sizeObservers.add(observer);
  }

  /**
   * Returns the BufferedImage object.
   *
   * @return the BufferedImage object.
   */
  public BufferedImage getBufferedImage() {
    return bufferedImage;
  }

  @Override
  public void update(ChaosGame chaosGame) {
    if (coloredCanvas) {
      renderColoredCanvas(chaosGame.getCanvas());
    } else {
      renderCanvas(chaosGame.getCanvas());
    }
  }
}
