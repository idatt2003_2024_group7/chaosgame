package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.io.ChaosGameFileHandler;
import edu.ntnu.idatt2003.group7.chaosgame.view.components.TopBar;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The TopBarController class is the controller for the top bar.
 * <p>
 * The class provides methods to connect the logic and the view of the top bar.
 * The top bar contains the menu bar and the fractal button.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class TopBarController {
  /**
   * The top bar object.
   */
  private final TopBar topBar;
  /**
   * The ChaosGameFileHandler object.
   */
  private final ChaosGameFileHandler chaosGameFileHandler;
  /**
   * The loadChaosGame object.
   */
  private final Consumer<ChaosGameDescription> loadChaosGame;
  /**
   * The FileChooser object.
   */
  private final FileChooser fileChooser;
  /**
   * The list of render color observers.
   */
  private final List<Consumer<Boolean>> renderColorObservers;
  /**
   * The list of dark canvas observers.
   */
  private final List<Consumer<Boolean>> darkCanvasObservers;
  /**
   * The list of detailed render observers.
   */
  private final List<Consumer<Boolean>> detailedRenderObservers;
  /**
   * The file path of the fractals' folder.
   */
  private static final String FILE_PATH = "src/main/resources/fractals";
  /**
   * The text extensions.
   */
  private static final String[] TXT_EXTENSIONS = new String[]{"Text Files (*.txt)", "*.txt"};
  /**
   * The png extensions.
   */
  private static final String[] PNG_EXTENSIONS = new String[]{"PNG Files (*.png)", "*.png"};
  /**
   * The jpeg extensions.
   */
  private static final String[] JPEG_EXTENSIONS = new String[]{"JPEG Files (*.jpeg)", "*.jpeg"};
  /**
   * The logger object.
   */
  private static final Logger logger = LoggerFactory.getLogger(TopBarController.class);

  /**
   * Initializes the top bar.
   * <p>
   * The method initializes the top bar by setting the actions
   * for the buttons and the menu items.
   * </p>
   */
  private void initialize() {
    topBar.getFractalButton().setOnAction(e -> {
    });

    // Create Fractal Menu
    topBar.getMenuBar().getSierpinskiTriangleMenuItem().setOnAction(e ->
        loadChaosGame.accept(ChaosGameDescriptionFactory.sierpinskiDescription()));
    topBar.getMenuBar().getBarnsleyFernMenuItem().setOnAction(e ->
        loadChaosGame.accept(ChaosGameDescriptionFactory.barnsleyDescription()));
    topBar.getMenuBar().getJuliaSetMenuItem().setOnAction(e ->
        loadChaosGame.accept(ChaosGameDescriptionFactory.juliaDescription()));

    // Load Fractal Menu
    topBar.getMenuBar().getLoadFromPathMenuItem().setOnAction(e -> loadFromFileChooser());
    createLoadFractalMenu();

    // Settings Menu
    fillSettingsMenu();
  }

  /**
   * Sets up the file chooser.
   * <p>
   * The method sets up the file chooser with the given directory and extensions.
   * </p>
   *
   * @param directory The directory.
   * @param extensions The extensions.
   */
  private void setupFileChooser(File directory, String[]... extensions) {
    fileChooser.setInitialDirectory(directory);
    fileChooser.getExtensionFilters().clear();
    fileChooser.getExtensionFilters().addAll(Stream.of(extensions)
        .map(ext -> new FileChooser.ExtensionFilter(ext[0], ext[1]))
        .toList());
  }

  /**
   * Loads a file from the file chooser.
   * <p>
   * The method loads a file from the file chooser and reads the file.
   * </p>
   */
  private void loadFromFileChooser() {
    setupFileChooser(null, TXT_EXTENSIONS);
    File file = fileChooser.showOpenDialog(new Stage());

    try {
      ChaosGameDescription description = chaosGameFileHandler.readFromFile(file.getAbsolutePath());
      loadChaosGame.accept(description);
    } catch (IOException e) {
      logger.atError().log("Error occurred when reading from file: " + e.getMessage());
    } catch (NullPointerException e) {
      logger.atError().log("Error occurred when reading from file: file cannot be null");
    } catch (IllegalArgumentException e) {
      logger.atError().log("Error occurred when creating ChaosGameDescription object: "
          + e.getMessage());
    }
  }

  /**
   * Saves the given ChaosGameDescription object.
   * <p>
   * The method saves the given ChaosGameDescription object by writing it to a file.
   * </p>
   *
   * @param description The ChaosGameDescription object.
   */
  private void save(ChaosGameDescription description) {
    File file = fileChooser.showSaveDialog(new Stage());
    try {
      chaosGameFileHandler.writeToFile(description, file.getAbsolutePath());
    } catch (IOException e) {
      logger.atError().log("Error occurred when writing to file: " + e.getMessage());
    } catch (NullPointerException e) {
      logger.atError().log("Error occurred when reading from file: file cannot be null");
    }
    createLoadFractalMenu();
  }

  /**
   * Creates the load fractal menu.
   * <p>
   * The method creates the load fractal menu by adding the fractal
   * files from the fractals folder to the menu.
   * </p>
   */
  private void createLoadFractalMenu() {
    try {
      List<String> fractalFiles = chaosGameFileHandler.getFractalFiles();
      topBar.getMenuBar().getLoadFromFolderMenu().getItems().clear();
      fractalFiles.forEach(fileName -> {
        MenuItem menuItem = new MenuItem(fileName);
        topBar.getMenuBar().getLoadFromFolderMenu().getItems().add(menuItem);
        menuItem.setOnAction(e -> {
          try {
            ChaosGameDescription description =
                chaosGameFileHandler.readFromFile(FILE_PATH + "/" + fileName);
            loadChaosGame.accept(description);
          } catch (IOException ioException) {
            // Log error message when button is pressed
            logger.atError().log("Error occurred when reading files: " + ioException.getMessage());
          } catch (IllegalArgumentException illegalArgumentException) {
            logger.atError().log("Error occurred when creating ChaosGameDescription object: "
                + illegalArgumentException.getMessage());
          }
        });
      });
    } catch (IOException e) {
      logger.atError().log("Error occurred when reading files: " + e.getMessage());
    }
  }

  /**
   * Fills the settings menu.
   * <p>
   * The method fills the settings menu by adding the render color, canvas color and render details.
   * </p>
   */
  private void fillSettingsMenu() {
    HBox renderColor = new HBox();
    CheckBox checkBoxRenderWithColor = new CheckBox();
    checkBoxRenderWithColor.setOnAction(e -> renderColorObservers.forEach(
        observer -> observer.accept(checkBoxRenderWithColor.isSelected())));
    renderColor.getChildren().addAll(checkBoxRenderWithColor, new Label("Render with color"));
    topBar.getMenuBar().getRenderColorMenuItem().setContent(renderColor);

    HBox canvasColor = new HBox();
    CheckBox checkBoxDarkCanvas = new CheckBox();
    checkBoxDarkCanvas.setOnAction(e -> darkCanvasObservers.forEach(
        observer -> observer.accept(checkBoxDarkCanvas.isSelected())));
    canvasColor.getChildren().addAll(checkBoxDarkCanvas, new Label("Dark canvas"));
    topBar.getMenuBar().getCanvasColorMenuItem().setContent(canvasColor);

    HBox renderDetails = new HBox();
    CheckBox checkBoxDetailedRender = new CheckBox();
    checkBoxDetailedRender.setOnAction(e -> detailedRenderObservers.forEach(
        observer -> observer.accept(checkBoxDetailedRender.isSelected())));
    renderDetails.getChildren().addAll(checkBoxDetailedRender, new Label("Detailed Julia render"));
    topBar.getMenuBar().getRenderDetailsMenuItem().setContent(renderDetails);
  }

  /**
   * Constructor for TopBarController,
   * creates a new TopBarController with the given top bar and loadChaosGame.
   * The constructor initializes the top bar and the file handler.
   *
   * @param topBar The top bar.
   * @param loadChaosGame The loadChaosGame consumer.
   */
  public TopBarController(TopBar topBar, Consumer<ChaosGameDescription> loadChaosGame) {
    this.topBar = topBar;
    this.loadChaosGame = loadChaosGame;
    chaosGameFileHandler = new ChaosGameFileHandler();
    fileChooser = new FileChooser();
    renderColorObservers = new ArrayList<>();
    darkCanvasObservers = new ArrayList<>();
    detailedRenderObservers = new ArrayList<>();
    initialize();
  }

  /**
   * Saves to a path chosen by the user.
   * <p>
   * The method saves the given ChaosGameDescription object using the file chooser.
   * </p>
   *
   * @param description The ChaosGameDescription object.
   */
  public void saveToFileChooser(ChaosGameDescription description) {
    setupFileChooser(null, TXT_EXTENSIONS);
    save(description);
  }

  /**
   * Saves to the fractals folder.
   * <p>
   * The method saves the given ChaosGameDescription object to the fractals folder.
   * </p>
   *
   * @param description The ChaosGameDescription object.
   */
  public void saveToFolder(ChaosGameDescription description) {
    setupFileChooser(new File(FILE_PATH), TXT_EXTENSIONS);
    save(description);
  }

  /**
   * Saves the image.
   * <p>
   * The method saves the fractal as an image using the file chooser.
   * </p>
   *
   * @param image The image.
   */
  public void saveImage(BufferedImage image) {
    setupFileChooser(null, PNG_EXTENSIONS, JPEG_EXTENSIONS);
    File file = fileChooser.showSaveDialog(new Stage());
    if (file != null) {
      try {
        chaosGameFileHandler.writeImageToFile(image, file.getAbsolutePath());
      } catch (IOException e) {
        logger.atError().log("Error occurred when writing image to file: " + e.getMessage());
      } catch (NullPointerException e) {
        logger.atError().log("Error occurred when reading from file: file cannot be null");
      }
    }
  }

  /**
   * Adds a render color observer.
   *
   * @param observer The observer.
   */
  public void addRenderColorObserver(Consumer<Boolean> observer) {
    renderColorObservers.add(observer);
  }

  /**
   * Adds a dark canvas observer.
   *
   * @param observer The observer.
   */
  public void addDarkCanvasObserver(Consumer<Boolean> observer) {
    darkCanvasObservers.add(observer);
  }

  /**
   * Adds a detailed render observer.
   *
   * @param observer The observer.
   */
  public void addDetailedRenderObserver(Consumer<Boolean> observer) {
    detailedRenderObservers.add(observer);
  }
}
