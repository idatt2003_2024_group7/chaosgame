package edu.ntnu.idatt2003.group7.chaosgame.model.math;

import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;

/**
 * A class representing a 2D vector.
 * <p>
 * The class provides methods for adding and subtracting vectors.
 * </p>
 *
 * @version 1.0
 * @since 0.1
 * @author Ylva Björnerstedt, Helene Kjerstad
 */
public class Vector2D {
  /**
   * The first component of the vector.
   */
  private final double x0;
  /**
   * The second component of the vector.
   */
  private final double x1;

  /**
   * Constructor for Vector2D, creates a new 2D vector with the given components.
   *
   * @param x0 The first component.
   * @param x1 The second component.
   */
  public Vector2D(double x0, double x1) throws IllegalArgumentException {
    try {
      validateDouble(x0);
      validateDouble(x1);

      this.x0 = x0;
      this.x1 = x1;
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create vector. " + e.getMessage());
    }
  }

  /**
   * Validates that the vector is a valid 2D vector.
   *
   * @param vector the vector to validate.
   *
   * @throws IllegalArgumentException if either of the parameters are null.
   */
  public void validateVector(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("The vector cannot be null");
    }
  }

  /**
   * Validates that the given value is a valid double.
   *
   * @param value the double to validate.
   * @throws IllegalArgumentException if the value is Not a Number (NaN).
   */
  public void validateDouble(double value) throws IllegalArgumentException {
    if (Double.isNaN(value)) {
      throw new IllegalArgumentException("The value is not a valid double");
    }
  }

  /**
   * Returns the first component of the vector.
   *
   * @return The first component of the vector.
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the second component of the vector.
   *
   * @return The second component of the vector.
   */
  public double getX1() {
    return x1;
  }

  /**
   * Adds a vector to the current vector.
   *
   * @param other the vector to add to the current vector.
   * @return a new vector that is the sum of the current vector and the other vector.
   * @throws IllegalArgumentException if the other vector is not valid.
   */
  public Vector2D add(Vector2D other) throws IllegalArgumentException {
    validateVector(other);

    return new Vector2D(x0 + other.x0, x1 + other.x1);
  }

  /**
   * Subtracts a vector to the current vector.
   *
   * @param other the vector to add to the current vector.
   * @return a new vector that is the difference of the current vector and the other vector.
   * @throws IllegalArgumentException if the other vector is not valid.
   */
  public Vector2D subtract(Vector2D other) throws IllegalArgumentException {
    validateVector(other);

    return new Vector2D(x0 - other.x0, x1 - other.x1);
  }

  /**
   * Scales the current vector by a scalar.
   *
   * @param scalar the scalar to scale the vector by.
   * @return a new vector that is the current vector scaled by the scalar.
   * @throws IllegalArgumentException if the scalar is not a valid double.
   */
  public Vector2D scale(double scalar) throws IllegalArgumentException {
    validateDouble(scalar);

    return new Vector2D(x0 * scalar, x1 * scalar);
  }

  /**
   * Compares the current vector to another object to determine if they are equal.
   *
   * @param o the object to compare to.
   * @return true if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (o instanceof Vector2D other) {
      return x0 == other.x0 && x1 == other.x1;
    }
    return false;
  }

  /**
   * Returns the string representation of the vector.
   *
   * @return the string representation of the vector.
   */
  @Override
  public String toString() {
    return String.format("%s, %s", Utility.doubleToString(x0), Utility.doubleToString(x1));
  }
}
