package edu.ntnu.idatt2003.group7.chaosgame;

import edu.ntnu.idatt2003.group7.chaosgame.view.App;

/**
 * The Main class for the Chaos Game application.
 *
 * @version 1.0
 * @since 0.1
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class Main {
  /**
   * The main method for the Chaos Game application.
   *
   * @param args the command-line arguments.
   */
  public static void main(String[] args) {
    // Uncomment the following lines to test the CLI version of the application,
    // and comment out App.main(args)
    /*ChaosGameCLI chaosGameCLI = new ChaosGameCLI();
    chaosGameCLI.start();*/
    App.main(args);
  }
}
