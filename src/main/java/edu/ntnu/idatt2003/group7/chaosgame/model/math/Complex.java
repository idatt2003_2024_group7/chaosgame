package edu.ntnu.idatt2003.group7.chaosgame.model.math;

/**
 * The Complex class represents a complex number with double components.
 * <p>
 * The class provides methods to calculate the square of the complex number.
 * The class inherits from {@link Vector2D}.
 * </p>
 *
 * @version 1.0
 * @since 0.1
 * @author Ylva Björnerstedt, Helene Kjerstad
 * @see Vector2D
 */
public class Complex extends Vector2D {

  /**
   * Constructor for Complex, creates a new complex number with the given components.
   * Inherits from Vector2D.
   *
   * @param realPart      The first component.
   * @param imaginaryPart The second component.
   * @throws IllegalArgumentException if the parameters are not a valid double.
   */
  public Complex(double realPart, double imaginaryPart) throws IllegalArgumentException {
    super(realPart, imaginaryPart);
  }

  /**
   * Calculates the square of the complex number.
   *
   * @return the square as a complex number.
   * @throws IllegalArgumentException if the value is not a valid double.
   */
  public Complex sqrt() throws IllegalArgumentException {
    double realPart =
        Math.sqrt(0.5 * (Math.sqrt(Math.pow(getX0(), 2) + Math.pow(getX1(), 2)) + getX0()));
    double imaginaryPart = Math.signum(getX1()) * Math.sqrt(
        0.5 * (Math.sqrt(Math.pow(getX0(), 2) + Math.pow(getX1(), 2)) - getX0()));
    return new Complex(realPart, imaginaryPart);
  }
}
