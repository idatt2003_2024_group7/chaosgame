package edu.ntnu.idatt2003.group7.chaosgame.model.fractal;

import edu.ntnu.idatt2003.group7.chaosgame.controller.ChaosGameObserver;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * The ChaosGame class represents a chaos game.
 * <p>
 * The class provides methods to run the game, update the game and get the canvas.
 * </p>
 *
 * @version 1.0
 * @since 0.2
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGame {
  /**
   * The ChaosCanvas object of the ChaosGame object.
   */
  private ChaosCanvas canvas;
  /**
   * The ChaosGameDescription object of the ChaosGame object.
   */
  private ChaosGameDescription description;
  /**
   * The current point of the ChaosGame object.
   */
  private Vector2D currentPoint;
  /**
   * The random object of the ChaosGame object.
   */
  private final Random random;
  /**
   * The list of observers of the ChaosGame object.
   */
  private final List<ChaosGameObserver> observers = new ArrayList<>();
  /**
   * The list of points of the already visited points.
   */
  private List<Vector2D> points;

  /**
   * The constructor of ChaosGame. Creates an instance of the class.
   *
   * @param description the ChaosGameDescription.
   * @param width the width of the canvas.
   * @param height the height of the canvas.
   * @throws IllegalArgumentException if any of the parameters are null.
   */
  public ChaosGame(ChaosGameDescription description, int width, int height)
      throws IllegalArgumentException {
    try {
      validateDescription(description);
      validateInt(width, "width");
      validateInt(height, "height");

      this.canvas = new ChaosCanvas(
          width, height, description.getMinCoords(), description.getMaxCoords());
      this.description = description;
      this.currentPoint = new Vector2D(0, 0);
      this.random = new Random();
      points = new ArrayList<>();
      points.add(currentPoint);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create ChaosGame object. " + e.getMessage());
    }
  }

  /**
   * Validates the ChaosGameDescription.
   *
   * @param description to be validated.
   * @throws  IllegalArgumentException if the description is null.
   */
  private void validateDescription(ChaosGameDescription description) {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
  }

  /**
   * Validates the integer.
   *
   * @param value the integer to be validated.
   * @param name the name of the integer.
   *
   * @throws IllegalArgumentException if the integer is invalid.
   */
  private void validateInt(int value, String name) {
    if (value <= 0) {
      throw new IllegalArgumentException(String.format("The %s must be a positive int", name));
    }
  }

  /**
   * Gets the canvas.
   *
   * @return the canvas as an object of ChaosCanvas.
   */
  public ChaosCanvas getCanvas()  {
    return this.canvas;
  }

  /**
   * Gets the current point.
   *
   * @return the current point as an object of Vector2D.
   */
  public Vector2D getCurrentPoint() {
    return this.currentPoint;
  }

  /**
   * Gets the description.
   *
   * @return the description as an object of ChaosGameDescription.
   */
  public ChaosGameDescription getDescription() {
    return description;
  }

  /**
   * Runs the game with a given amount of steps and a queue of points.
   *
   * @param steps the amount of steps
   * @param pointQueue the queue of points
   */
  private void runWithQueue(int steps, Deque<Vector2D> pointQueue) {
    int count = 0;
    while (!pointQueue.isEmpty() && count < steps) {
      count++;
      Vector2D point = pointQueue.getFirst();
      if (canvas.isWithinCanvas(point)) {
        description.getTransforms().forEach(transform -> doTransform(point, transform, pointQueue));
      }
    }
  }

  /**
   * Does a transformation on a point and
   * fills in the pixel at the new point if it is within the canvas.
   * If the new point is not already in the list of points, it is added to the list and the queue.
   *
   * @param point the point to be transformed
   * @param transform the transformation to be applied
   * @param pointQueue the queue of points
   */
  private void doTransform(Vector2D point, Transform2D transform, Deque<Vector2D> pointQueue) {
    Vector2D newPoint = transform.transform(point);
    if (canvas.isWithinCanvas(newPoint)) {
      canvas.setPixel(newPoint);
      if (!points.contains(newPoint)) {
        points.add(newPoint);
        pointQueue.push(newPoint);
      }
    }
  }

  /**
   * Runs the game with step amounts of rounds. Begins by filling in the pixel at current position,
   * then for each round rolls a die to choose transformation.
   * Then calculates new position using transformation
   * before filling in the pixel.
   *
   * @param steps the amount of rounds
   * @throws IllegalArgumentException if steps is invalid
   */
  public void runSteps(int steps) throws IllegalArgumentException {
    validateInt(steps, "steps");

    canvas.setPixel(currentPoint);
    int die;
    for (int i = 0; i < steps; i++) {
      die = random.nextInt(0, description.getTransforms().size());
      currentPoint = description.getTransforms().get(die).transform(currentPoint);
      if (canvas.isWithinCanvas(currentPoint)) {
        canvas.setPixel(currentPoint);
      }
    }
  }

  /**
   * Adds the {@code runSteps} method to the executor and runs in a new thread.
   *
   * @param steps the amount of rounds
   * @param executor the executor to run the method
   *
   * @return a list of futures, for the runnables added to the executor
   * @throws IllegalArgumentException if steps is invalid
   */
  public List<Future<Void>> runSteps(int steps, ExecutorService executor)
      throws IllegalArgumentException {
    currentPoint = new Vector2D(0, 0);
    List<Future<Void>> futures = new ArrayList<>();
    try {
      futures.add(executor.submit(() -> {
        runSteps(steps);
        return null;
      }));
      executor.shutdown();
    } catch (Exception e) {
      throw new IllegalArgumentException("Unable to run chaos game. " + e.getMessage());
    }
    return futures;
  }

  /**
   * Runs a modified calculation of the Julia set.
   * <p>
   * Submits the {@code runWithQueue} method twice with different parameters to the executor
   * and runs in two new threads.
   * Submits the {@code runSteps} method to the executor and runs in a new thread.
   * </p>
   *
   * @param steps the amount of rounds
   * @param executor the executor to run the methods
   *
   * @return a list of futures, for the runnables added to the executor
   */
  public List<Future<Void>> runModifiedJulia(int steps, ExecutorService executor)
      throws IllegalArgumentException {
    this.points = new ArrayList<>();
    currentPoint = new Vector2D(0, 0);
    List<Future<Void>> futures = new ArrayList<>();
    try {
      description.getTransforms().forEach(transform -> {
        Deque<Vector2D> pointQueue = new ArrayDeque<>();
        pointQueue.push(transform.transform(currentPoint));
        futures.add(executor.submit(() -> {
          runWithQueue(steps, pointQueue);
          return null;
        }));
      });
      futures.add(executor.submit(() -> {
        runSteps(steps);
        return null;
      }));
      executor.shutdown();
    } catch (Exception e) {
      throw new IllegalArgumentException("Unable to run chaos game. " + e.getMessage());
    }
    return futures;
  }

  /**
   * Updates the ChaosGame with a new description, width and height.
   *
   * @param description the new chaos game description
   * @param width the new width of the canvas
   * @param height the new height of the canvas
   *
   * @throws IllegalArgumentException if any of the parameters are invalid
   */
  private void updateChaosGame(ChaosGameDescription description, int width, int height)
      throws IllegalArgumentException {
    try {
      validateDescription(description);
      validateInt(width, "width");
      validateInt(height, "height");
      this.description = description;
      this.canvas = new ChaosCanvas(
          width, height, description.getMinCoords(), description.getMaxCoords());
      this.currentPoint = new Vector2D(0, 0);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to update ChaosGame object. " + e.getMessage());
    }
  }

  /**
   * Updates the ChaosGame with a new description.
   *
   * @param description the new chaos game description
   *
   * @throws IllegalArgumentException if the description is invalid
   */
  public void updateChaosGameDescription(ChaosGameDescription description)
      throws IllegalArgumentException {
    updateChaosGame(description, canvas.getWidth(), canvas.getHeight());
  }

  /**
   * Updates the ChaosGame with a new width and height.
   *
   * @param width the new width of the canvas
   * @param height the new height of the canvas
   *
   * @throws IllegalArgumentException if the width or height is invalid
   */
  public void updateCanvasSize(int width, int height) throws IllegalArgumentException {
    updateChaosGame(description, width, height);
  }

  /**
   * Updates the ChaosGame with a new size.
   *
   * @param size the vector containing the new width and height of the canvas
   */
  public void updateCanvasSize(Vector2D size) throws IllegalArgumentException {
    updateCanvasSize((int) size.getX0(), (int) size.getX1());
  }

  /**
   * Adds an observer to the list of observers.
   *
   * @param observer to be added
   */
  public void addObserver(ChaosGameObserver observer) {
    observers.add(observer);
  }

  /**
   * Removes an observer from the list of observers.
   *
   * @param observer to be removed
   */
  public void removeObserver(ChaosGameObserver observer) {
    observers.remove(observer);
  }

  /**
   * Notifies all observers in the list of observers.
   */
  public void notifyObservers() {
    observers.forEach(observer -> observer.update(this));
  }
}
