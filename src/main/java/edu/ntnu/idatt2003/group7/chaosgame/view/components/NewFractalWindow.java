package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * The NewFractalWindow class represents the window for creating a new fractal.
 *
 * @version 1.0
 * @since 0.3
 * @see VBox
 * @author Ylva Björnerstedt, Helene Kjerstad
 */
public class NewFractalWindow extends VBox {
  /**
   * The affine table for the new fractal window.
   */
  private GridPane affineTable;
  /**
   * The close button for the new fractal window.
   */
  private Button close;
  /**
   * The save affine button for the new fractal window.
   */
  private Button saveAffine;
  /**
   * The save julia button for the new fractal window.
   */
  private Button saveJulia;
  /**
   * The add row button for the new fractal window.
   */
  private Button addRow;
  /**
   * The text fields for the affine fractal.
   */
  private List<List<TextField>> affineTextFields;
  /**
   * The text fields for the julia fractal.
   */
  private List<List<TextField>> juliaTextFields;
  /**
   * The text field for the julia min.
   */
  private TextField juliaMinField;
  /**
   * The text field for the julia max.
   */
  private TextField juliaMaxField;
  /**
   * The text field for the affine min.
   */
  private TextField affineMinField;
  /**
   * The text field for the affine max.
   */
  private TextField affineMaxField;
  /**
   * The notification for the new fractal window.
   */
  private Notification notification;

  /**
   * Constructor for NewFractalWindow.
   */
  public NewFractalWindow() {
    makeTab();
  }

  /**
   * Makes the tab for the new fractal window.
   */
  private void makeTab() {
    this.getStyleClass().add("new-fractal-window-tab");

    // Make tabs for affine and julia fractals
    Tab affineTab = new Tab("Affine");
    Tab juliaTab = new Tab("Julia");

    // Make them not closable and set content
    affineTab.setClosable(false);
    juliaTab.setClosable(false);
    affineTab.setContent(affine());
    juliaTab.setContent(julia());

    TabPane tabPane = new TabPane();
    tabPane.getTabs().add(affineTab);
    tabPane.getTabs().add(juliaTab);
    this.getChildren().addAll(topRow(), tabPane);
    makeNotificationArea();
  }

  /**
   * Makes the notification area.
   */
  private void makeNotificationArea() {
    notification = new Notification();
    notification.getStyleClass().add("notification-area");
  }

  /**
   * Makes the top row of the new fractal window.
   *
   * @return the top row as a HBox
   */
  private HBox topRow() {
    Label title = new Label("New Fractal");
    title.getStyleClass().add("title-label");

    // Create close button and set alignment
    close = new Button("X");
    close.getStyleClass().add("close-button");
    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);

    HBox topRow = new HBox();
    topRow.getStyleClass().add("row");
    topRow.getChildren().addAll(title, spacer, close);
    return topRow;
  }

  /**
   * Makes the julia fractal content.
   *
   * @return the julia fractal content as a VBox
   */
  private VBox julia() {
    juliaTextFields = new ArrayList<>();

    VBox container = new VBox();
    container.getStyleClass().add("row");
    HBox fieldsContainer = new HBox();
    fieldsContainer.getStyleClass().add("fractal-container");

    // Create labels and textfields for the transformation
    TextField a = new TextField();
    a.getStyleClass().add("small-textField");
    a.setPromptText("a");
    TextField b = new TextField();
    b.getStyleClass().add("small-textField");
    b.setPromptText("b");

    List<TextField> textFields = new ArrayList<>();
    textFields.add(a);
    textFields.add(b);
    juliaTextFields.add(textFields);

    Label c = new Label("c = ");
    Label plus = new Label(" + ");
    Label i = new Label(" i");
    fieldsContainer.getChildren().addAll(c, a, plus,  b, i);

    HBox minMaxContainer = new HBox();
    minMaxContainer.getStyleClass().add("row");
    juliaMinField = new TextField();
    juliaMaxField = new TextField();
    minMaxContainer.getChildren().addAll(minCanvas(juliaMinField), maxCanvas(juliaMaxField));
    container.getChildren().addAll(fieldsContainer, minMaxContainer, bottomRowJulia());
    return container;
  }

  /**
   * Makes the affine fractal content.
   *
   * @return the affine fractal content as a VBox
   */
  private VBox affine() {
    affineTextFields = new ArrayList<>();
    VBox container = new VBox();
    container.getStyleClass().add("row");

    ScrollPane affine = new ScrollPane();
    affine.getStyleClass().add("fractal-container");
    VBox affineContainer = new VBox();
    affineContainer.getStyleClass().add("row");
    affine.setFitToWidth(true);

    // Set up grid table for adding affine transformations
    affineTable = new GridPane();
    affineTable.getStyleClass().add("affine-table");

    // Set up header for table
    affineTableHeader();

    affineTable.setHgap(5);
    affineTable.setVgap(5);

    // Add all to container
    affineContainer.getChildren().add(affineTable);
    affine.setContent(affineContainer);

    HBox minMaxContainer = new HBox();
    minMaxContainer.getStyleClass().add("row");
    affineMaxField = new TextField();
    affineMinField = new TextField();
    minMaxContainer.getChildren().addAll(minCanvas(affineMinField), maxCanvas(affineMaxField));
    container.getChildren().addAll(affine, minMaxContainer, bottomRowAffine());
    return container;
  }

  /**
   * Sets up the header for the affine table.
   */
  public void affineTableHeader() {
    // Make label
    Label a = new Label("A:");
    a.getStyleClass().add("affine-label");
    Label b = new Label("b:");
    b.getStyleClass().add("affine-label");

    // Add labels, set spacing for gridpane and add 1.st row
    affineTable.add(a, 0, 1);
    affineTable.add(b, 1, 1);
  }

  /**
   * Makes the min canvas input field.
   *
   * @param textField the text field to be added
   * @return the min canvas input as a HBox
   */
  private HBox minCanvas(TextField textField) {
    HBox row = new HBox();
    row.getStyleClass().add("row");
    Label min = new Label("Min:");

    textField.getStyleClass().add("medium-textField");
    textField.setPromptText("x, y");

    row.getChildren().addAll(min, textField);
    HBox.setHgrow(textField, Priority.ALWAYS);
    return row;
  }

  /**
   * Makes the max canvas input field.
   *
   * @param textField the text field to be added
   * @return the max canvas input as a HBox
   */
  private HBox maxCanvas(TextField textField) {
    HBox row = new HBox();
    row.getStyleClass().add("row");
    Label max = new Label("Max:");

    textField.getStyleClass().add("medium-textField");
    textField.setPromptText("x, y");

    row.getChildren().addAll(max, textField);
    HBox.setHgrow(textField, Priority.ALWAYS);
    return row;
  }

  /**
   * Makes the bottom row for the affine fractal.
   *
   * @return the bottom row as a HBox
   */
  private HBox bottomRowAffine() {
    HBox bottomRow = new HBox();
    bottomRow.getStyleClass().add("bottom-row");
    saveAffine = new Button("Continue");
    addRow = new Button("+ Row");
    bottomRow.getChildren().addAll(addRow, saveAffine);
    return bottomRow;
  }

  /**
   * Makes the bottom row for the julia fractal.
   *
   * @return the bottom row as a HBox
   */
  private HBox bottomRowJulia() {
    HBox bottomRow = new HBox();
    bottomRow.getStyleClass().add("bottom-row");
    bottomRow.getStyleClass().add("row");
    saveJulia = new Button("Continue");
    bottomRow.getChildren().add(saveJulia);
    return bottomRow;
  }

  /**
   * Adds a row to the affine table.
   *
   * @param row the row index to add the row to
   */
  public void addAffineRow(int row) {
    // Create text fields
    TextField a = new TextField();
    a.setPromptText("a00, a01, a10, a11");
    a.getStyleClass().add("affine-textField");
    TextField b = new TextField();
    b.setPromptText("x, y");
    b.getStyleClass().add("affine-textField");

    List<TextField> textFields = new ArrayList<>();
    textFields.add(a);
    textFields.add(b);
    affineTextFields.add(textFields);

    // Insert into gridpane
    affineTable.add(a, 0, row);
    affineTable.add(b, 1, row);
  }

  /**
   * Adds a row to the affine table with a remove button.
   *
   * @param row the row index to add the row to
   * @param remove the remove button to add
   */
  public void addRemoveAffineRow(int row, Button remove) {
    // Add row with remove button
    addAffineRow(row);
    affineTable.add(remove, 2, row);
  }

  /**
   * Adds a notification to the new fractal window.
   */
  public void addNotification() {
    this.setMaxHeight(550);
    this.getChildren().add(notification);
  }

  /**
   * Removes the notification from the new fractal window.
   */
  public void removeNotification() {
    this.setMaxHeight(490);
    this.getChildren().remove(notification);
  }

  /**
   * Returns the close button.
   *
   * @return the close button
   */
  public Button getCloseButton() {
    return close;
  }

  /**
   * Returns the number of rows in the affine table.
   *
   * @return the number of rows in the affine table
   */
  public int getRowCount() {
    return affineTable.getRowCount();
  }

  /**
   * Returns the affine table.
   *
   * @return the affine table
   */
  public GridPane getAffineTable() {
    return affineTable;
  }

  /**
   * Returns the save julia button.
   *
   * @return the save julia button
   */
  public Button getSaveJulia() {
    return saveJulia;
  }

  /**
   * Returns the save affine button.
   *
   * @return the save affine button
   */
  public Button getSaveAffine() {
    return saveAffine;
  }

  /**
   * Returns the add row button.
   *
   * @return the add row button
   */
  public Button getAddRow() {
    return addRow;
  }

  /**
   * Returns the affine text fields.
   *
   * @return the affine text fields
   */
  public List<List<TextField>> getAffineTextFields() {
    return affineTextFields;
  }

  /**
   * Returns the julia text fields.
   *
   * @return the julia text fields
   */
  public List<List<TextField>> getJuliaTextFields() {
    return  juliaTextFields;
  }

  /**
   * Returns the julia min field.
   *
   * @return the julia min field
   */
  public TextField getJuliaMinField() {
    return juliaMinField;
  }

  /**
   * Returns the julia max field.
   *
   * @return the julia max field
   */
  public TextField getJuliaMaxField() {
    return juliaMaxField;
  }

  /**
   * Returns the affine min field.
   *
   * @return the affine min field
   */
  public TextField getAffineMinField() {
    return affineMinField;
  }

  /**
   * Returns the affine max field.
   *
   * @return the affine max field
   */
  public TextField getAffineMaxField() {
    return affineMaxField;
  }

  /**
   * Returns the notification box.
   *
   * @return the notification box
   */
  public Notification getNotificationBox() {
    return notification;
  }
}
