package edu.ntnu.idatt2003.group7.chaosgame.utility;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import java.util.Locale;
import java.util.Scanner;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * The Utility class provides utility methods for the Chaos Game application.
 *
 * @version 1.0
 * @since 0.2
 * @author Helene Kjerstad, Ylva Björnerstedt
*/
public class Utility {

  /**
   * Private constructor to prevent instantiation.
   */
  private Utility() {}

  /**
   * Reads an integer from the scanner, and returns it if it is within the specified range.
   * The range is from 1 to the upper bound, inclusive.
   *
   * @param scanner the scanner to read from
   * @param upperBound the upper bound of the integer, inclusive
   * @return the integer read from the scanner
   */
  public static int getNewIntRange(Scanner scanner, int upperBound) {
    if (scanner.hasNextInt()) {
      int i = scanner.nextInt();
      if (i > 0 && i <= upperBound) {
        return i;
      }
    } else {
      scanner.next();
    }
    System.out.println("Please enter a valid number between 1 and " + upperBound);
    return getNewIntRange(scanner, upperBound);
  }

  /**
   * Reads a vector from the scanner.
   *
   * @param scanner the scanner to read from
   * @return the vector read from the scanner
   */
  public static Vector2D readVector(Scanner scanner) {
    return new Vector2D(scanner.nextDouble(), scanner.nextDouble());
  }

  /**
   * Reads a matrix from the scanner.
   *
   * @param scanner the scanner to read from
   * @return the matrix read from the scanner
   */
  public static Matrix2x2 readMatrix(Scanner scanner) {
    return new Matrix2x2(
        scanner.nextDouble(), scanner.nextDouble(),
        scanner.nextDouble(), scanner.nextDouble()
    );
  }

  /**
   * Returns a scanner with the given scanner's next line as input.
   *
   * @param scanner the scanner to get the next line from
   * @return a scanner with the given scanner's next line as input
   */
  public static Scanner getInlineScanner(Scanner scanner) {
    Scanner scannerInline = new Scanner(scanner.nextLine());
    scannerInline.useDelimiter("[,\\s]+");
    scannerInline.useLocale(Locale.ENGLISH);
    return scannerInline;
  }

  /**
   * Converts a double to a string.
   *
   * @param d the double to convert
   * @return the string representation of the double
   */
  public static String doubleToString(double d) {
    String s = String.valueOf(d);

    if (s.endsWith(".0")) {
      s = s.substring(0, s.length() - 2);
    } else if (s.startsWith("-0")) {
      s = "-" + s.substring(s.indexOf('.'));
    } else if (s.startsWith("0")) {
      s = s.substring(s.indexOf('.'));
    }
    return s;
  }

  /**
   * Writes a line with the given value and comment.
   *
   * @param value the value
   * @param comment the comment
   * @param indent the indent
   * @return the line with the given value and comment
   */
  public static String writeLine(String value, String comment, int indent) {
    return value + " ".repeat(indent - value.length()) + "# " + comment;
  }

  /**
   * Returns a normalized value based on the given value.
   *
   * @param value the value
   * @param min the minimum value
   * @param max the maximum value
   * @return the normalized value based on the given value
   */
  public static double getNormalizedValue(int value, int min, int max) {
    return (double) (value - min) / (max - min);
  }

  /**
   * Returns a color based on the given value.
   *
   * @param value the value
   * @param min the minimum value
   * @param max the maximum value
   * @return the color based on the given value
   */
  public static Color getColorFromValue(int value, int min, int max) {
    double normalizedValue = getNormalizedValue(value, min, max);
    double hue = 240 - normalizedValue * 180;
    // Hue values from 240 to 60 represent blue to yellow to red
    return Color.hsb(hue, 1.0, 1.0);
    //return return Color.rgb((int) (255 * normalizedValue),0, (int) (255 * (1 - normalizedValue)));
  }

  /**
   * Converts a color to an RGB value.
   *
   * @param color the color to convert
   * @return the RGB value of the color
   */
  public static int colorToRGB(Color color) {
    return (int) (color.getRed() * 255) << 16
        | (int) (color.getGreen() * 255) << 8 | (int) (color.getBlue() * 255);
  }

  /**
   * Returns a scanner with the given input.
   *
   * @param input the input
   * @return a scanner with the given input
   */
  public static Scanner getScanner(String input) {
    Scanner scanner = new Scanner(input);
    scanner.useDelimiter("[,\\s]+");
    scanner.useLocale(Locale.ENGLISH);
    return scanner;
  }

  /**
   * Parses a matrix from the given string.
   *
   * @param matrix the string to parse in the format "a00, a01, a10, a11"
   * @return the matrix parsed from the given string
   * @throws IllegalArgumentException if the string is invalid
   */
  public static Matrix2x2 parseMatrix(String matrix)throws IllegalArgumentException {
    try {
      return readMatrix(getScanner(matrix));
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Invalid input for matrix: Please enter a number");
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Invalid input for matrix: Please enter 4 values separated by a comma");
    }
  }

  /**
   * Parses a vector from the given string.
   *
   * @param vector the string to parse in the format "x, y"
   * @return the vector parsed from the given string
   * @throws IllegalArgumentException if the string is invalid
   */
  public static Vector2D parseVector(String vector) throws IllegalArgumentException {
    try {
      return readVector(getScanner(vector));
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Please enter a number");
    } catch (Exception e) {
      throw new IllegalArgumentException("Please enter 2 values separated by a comma");
    }
  }

  /**
   * Parses a double from the given text field.
   *
   * @param textField the text field to parse
   * @return the double parsed from the given text field
   * @throws IllegalArgumentException if the input is invalid
   */
  public static double parseDoubleTextField(TextField textField) throws IllegalArgumentException {
    try {
      return Double.parseDouble(textField.getText());
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Invalid input for transformation: Please enter a number");
    } catch (Exception e) {
      throw new IllegalArgumentException("Invalid input for transformation: " + e.getMessage());
    }
  }

  /**
   * Parses an integer from the given text field.
   *
   * @param textField the text field to parse
   * @return the integer parsed from the given text field
   * @throws IllegalArgumentException if the input is invalid
   */
  public static int parseStepsTextField(TextField textField) throws IllegalArgumentException {
    try {
      int steps = Integer.parseInt(textField.getText());
      if (steps < 1) {
        throw new IllegalArgumentException(
            "Invalid input for steps: Please enter an integer greater than 0");
      }
      return steps;
    } catch (NumberFormatException e) {
      throw new NumberFormatException("Invalid input for steps: Please enter an integer");
    } catch (Exception e) {
      throw new IllegalArgumentException("Invalid input for steps: " + e.getMessage());
    }
  }
}
