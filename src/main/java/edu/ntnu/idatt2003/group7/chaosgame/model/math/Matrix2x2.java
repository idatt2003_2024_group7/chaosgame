package edu.ntnu.idatt2003.group7.chaosgame.model.math;

import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;

/**
 * The Matrix2x2 class represents a 2x2 matrix with double components.
 * <p>
 * The class provides methods to multiply the matrix with a vector.
 * </p>
 *
 * @version 1.0
 * @since 0.1
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class Matrix2x2 {
  /**
   * The upper left component of the matrix.
   */
  private final double a00;
  /**
   * The upper right component of the matrix.
   */
  private final double a01;
  /**
   * The lower left component of the matrix.
   */
  private final double a10;
  /**
   * The lower right component of the matrix.
   */
  private final double a11;

  /**
   * Validates that the given value is a valid double.
   *
   * @param value the double to validate.
   * @param name  the name of the double.
   *
   * @throws IllegalArgumentException if the value is Not a Number (NaN).
   */
  private void validateDouble(double value, String name) throws IllegalArgumentException {
    if (Double.isNaN(value)) {
      throw new IllegalArgumentException(name + " is not a valid double");
    }
  }

  /**
   * Constructor for Matrix2x2, creates a new 2x2 matrix with the given components.
   *
   * @param a00 The first component.
   * @param a01 The second component.
   * @param a10 The third component.
   * @param a11 The fourth component.
   *
   * @throws IllegalArgumentException if the parameters are not valid doubles.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) throws IllegalArgumentException {
    try {
      validateDouble(a00, "a00");
      validateDouble(a01, "a01");
      validateDouble(a10, "a10");
      validateDouble(a11, "a11");

      this.a00 = a00;
      this.a01 = a01;
      this.a10 = a10;
      this.a11 = a11;
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create Matrix2x2 object. " + e.getMessage());
    }
  }

  /**
   * Multiplies the matrix with a vector.
   *
   * @param vector the vector to multiply with the matrix.
   *
   * @return a new vector that is the product of the matrix and the vector.
   *
   * @throws IllegalArgumentException if the vector is null.
   */
  public Vector2D multiply(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("The vector cannot be null");
    }
    return new Vector2D(a00 * vector.getX0() + a01 * vector.getX1(),
        a10 * vector.getX0() + a11 * vector.getX1());
  }

  /**
   * Returns the string representation of the matrix.
   *
   * @return the string representation of the matrix.
   */
  @Override
  public String toString() {
    return String.format("%s, %s, %s, %s", Utility.doubleToString(a00), Utility.doubleToString(a01),
        Utility.doubleToString(a10), Utility.doubleToString(a11));
  }
}
