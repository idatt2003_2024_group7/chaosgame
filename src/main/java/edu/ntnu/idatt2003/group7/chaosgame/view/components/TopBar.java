package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 * The TopBar class for the Chaos Game application.
 * <p>
 * This class is a custom {@link StackPane} that contains the MainMenuBar
 * and the fractal button of the TopBar.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see StackPane
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class TopBar extends StackPane {
  /**
   * The MainMenuBar of the TopBar.
   */
  private MainMenuBar menuBar;
  /**
   * The fractal button of the TopBar.
   */
  private Button fractalButton;

  private BorderPane menuBorderPane;

  private BorderPane fractalBorderPane;

  /**
   * The constructor for the TopBar.
   */
  public TopBar() {
    make();
  }

  /**
   * Returns the MainMenuBar of the TopBar.
   *
   * @return the MainMenuBar of the TopBar.
   */
  public MainMenuBar getMenuBar() {
    return menuBar;
  }

  /**
   * Returns the fractal button of the TopBar.
   *
   * @return the fractal button of the TopBar.
   */
  public Button getFractalButton() {
    return fractalButton;
  }

  /**
   * Creates the TopBar.
   */
  private void make() {
    menuBorderPane = new BorderPane();
    fractalBorderPane = new BorderPane();
    menuBar = new MainMenuBar();
    fractalButton = new Button("Edit fractal");
    fractalButton.getStyleClass().add("menu-bar-button");

    fractalBorderPane.setPickOnBounds(false);

    fractalButton.getStyleClass().add("border-pane-button");
    this.getStyleClass().add("top-bar");

    menuBorderPane.setLeft(menuBar);
    fractalBorderPane.setRight(fractalButton);
    this.getChildren().addAll(menuBorderPane, fractalBorderPane);
  }
}
