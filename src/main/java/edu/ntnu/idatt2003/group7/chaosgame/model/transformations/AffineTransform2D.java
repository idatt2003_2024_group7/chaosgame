package edu.ntnu.idatt2003.group7.chaosgame.model.transformations;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;

/**
 * The AffineTransform2D class represents a 2D affine transformation.
 * <p>
 * The class provides methods to transform a point as the Affine transformation.
 * the class implements the {@link Transform2D} interface.
 * </p>
 *
 * @version 1.0
 * @since 0.1
 * @author Helene Kjerstad, Ylva Björnerstedt
 * @see Transform2D
 */
public class AffineTransform2D implements Transform2D {
  /**
   * The 2x2 matrix of the AffineTransform2D object.
   */
  private final Matrix2x2 matrix;
  /**
   * The 2D vector of the AffineTransform2D object.
   */
  private final Vector2D vector;

  /**
   * Validates that the matrix is a valid 2x2 matrix.
   *
   * @param matrix The 2x2 matrix.
   *
   * @throws IllegalArgumentException if the matrix is not valid.
   */
  private void validateMatrix(Matrix2x2 matrix) throws IllegalArgumentException {
    if (matrix == null) {
      throw new IllegalArgumentException("The matrix cannot be null");
    }
  }

  /**
   * Validates that the vector is a valid 2D vector.
   *
   * @param vector The 2D vector.
   * @param name The name of the parameter.
   *
   * @throws IllegalArgumentException if the vector is not valid.
   */
  private void validateVector(Vector2D vector, String name) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException(String.format("The %s cannot be null", name));
    }
  }

  /**
   * Constructor for AffineTransform2D,
   * creates a new 2D affine transformation with the given matrix and vector.
   *
   * @param matrix The 2x2 matrix.
   * @param vector The 2D vector.
   *
   * @throws IllegalArgumentException if the matrix or vector is null.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) throws IllegalArgumentException {
    try {
      validateMatrix(matrix);
      validateVector(vector, "vector");
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create AffineTransform2D object. "
          + e.getMessage());
    }

    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Returns the matrix of the AffineTransform2D object.
   *
   * @return the matrix of the AffineTransform2D object.
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of the AffineTransform2D object.
   *
   * @return the vector of the AffineTransform2D object.
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * Transforms the given point as the Affine transformation.
   *
   * @param point the point to be transformed
   *
   * @return the transformed point
   *
   * @throws IllegalArgumentException if the point is null
   */
  @Override
  public Vector2D transform(Vector2D point) throws IllegalArgumentException {
    validateVector(point, "point");
    return matrix.multiply(point).add(vector);
  }

  /**
   * Returns a string representation of the AffineTransform2D object.
   *
   * @return a string representation of the AffineTransform2D object.
   */
  @Override
  public String toString() {
    return String.format("%s, %s", matrix, vector);
  }
}
