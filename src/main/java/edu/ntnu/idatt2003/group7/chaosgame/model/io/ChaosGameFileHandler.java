package edu.ntnu.idatt2003.group7.chaosgame.model.io;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

/**
 * The ChaosGameFileHandler class provides methods to read and write
 * ChaosGameDescription objects from and to files.
 *
 * @version 1.0
 * @since 0.2
 * @see ChaosGameDescription
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGameFileHandler {
  private static final String DIRECTORY = "src/main/resources/fractals";

  /**
   * Constructor for ChaosGameFileHandler.
   */
  public ChaosGameFileHandler() {}

  /**
   * Reads a ChaosGameDescription object from a file.
   *
   * @param path the path to the file
   * @return a ChaosGameDescription object
   * @throws IOException if an I/O error occurs
   * @throws IllegalArgumentException if the file contains invalid data
   */
  public ChaosGameDescription readFromFile(String path)
      throws IOException, IllegalArgumentException {
    Vector2D minCoords = null;
    Vector2D maxCoords = null;
    List<Transform2D> transformations = new ArrayList<>();
    Scanner scannerInline;
    String transformationType;

    try (Scanner scanner = new Scanner(Path.of(path))) {
      while (scanner.hasNextLine()) {
        // Read transformation type
        scannerInline = Utility.getInlineScanner(scanner);
        transformationType = scannerInline.next();

        // Read min and max coordinates
        scannerInline = Utility.getInlineScanner(scanner);
        minCoords = Utility.readVector(scannerInline);
        scannerInline = Utility.getInlineScanner(scanner);
        maxCoords = Utility.readVector(scannerInline);

        // Read transformations
        if (transformationType.equals("Affine2D")) {
          while (scanner.hasNextLine()) {
            scannerInline = Utility.getInlineScanner(scanner);
            AffineTransform2D transform = new AffineTransform2D(
                Utility.readMatrix(scannerInline), Utility.readVector(scannerInline));
            transformations.add(transform);
          }
        } else if (transformationType.equals("Julia")) {
          while (scanner.hasNextLine()) {
            scannerInline = Utility.getInlineScanner(scanner);
            Complex c = new Complex(scannerInline.nextDouble(), scannerInline.nextDouble());
            JuliaTransform transformPos = new JuliaTransform(c, 1);
            JuliaTransform transformNeg = new JuliaTransform(c, -1);
            transformations.add(transformPos);
            transformations.add(transformNeg);
          }
        }
      }
    } catch (IOException e) {
      throw new IOException("Unable to read data from " + path + "\ndue to " + e.getMessage());
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create object form data in " + path
          + "\ndue to " + e.getMessage());
    }
    return new ChaosGameDescription(transformations, minCoords, maxCoords);
  }

  /**
   * Writes a ChaosGameDescription object to a file.
   *
   * @param description the ChaosGameDescription object
   * @param path the path to the file
   * @throws IOException if an I/O error occurs
   * @throws IllegalArgumentException if the description is null
   */
  public void writeToFile(ChaosGameDescription description, String path)
      throws IOException, IllegalArgumentException {
    if (description == null) {
      throw new IllegalArgumentException("Description cannot be null");
    }
    try (BufferedWriter writer = Files.newBufferedWriter(Path.of(path))) {
      writer.write(description.toString());
    } catch (IOException e) {
      throw new IOException("Unable to write data to " + path + "\ndue to " + e.getMessage(), e);
    }
  }

  /**
   * Gets a list of the fractal files in the resources directory.
   *
   * @return a list of the file names
   *
   * @throws IOException if an I/O error occurs
   */
  public List<String> getFractalFiles() throws IOException {
    Path resourcesDirectory = Paths.get(DIRECTORY);
    List<String> pathNames = new ArrayList<>();
    try (Stream<Path> paths = Files.walk(resourcesDirectory, 1)) {
      paths
          .filter(Files::isRegularFile)
          .forEach(path -> pathNames.add(path.getFileName().toString()));
    } catch (IOException e) {
      throw new IOException("Unable to get fractal files from " + DIRECTORY + "\ndue to "
          + e.getMessage(), e);
    }
    return pathNames;
  }

  /**
   * Writes an image to a file.
   *
   * @param image the image to be written
   * @param path the path to the file
   * @throws IOException if an I/O error occurs
   * @throws IllegalArgumentException if the file format is unsupported
   */
  public void writeImageToFile(BufferedImage image, String path)
      throws IOException, IllegalArgumentException {
    try {
      String fileExtension = Files.probeContentType(Path.of(path));
      if (fileExtension.equals("image/png")) {
        ImageIO.write(image, "png", new File(path));
      } else if (fileExtension.equals("image/jpeg")) {
        ImageIO.write(image, "jpeg", new File(path));
      } else {
        throw new IllegalArgumentException("Unsupported file format");
      }
    } catch (IOException e) {
      throw new IOException("Unable to write image to " + path + "\ndue to " + e.getMessage(), e);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to write image to " + path + "\ndue to "
          + e.getMessage(), e);
    }
  }
}
