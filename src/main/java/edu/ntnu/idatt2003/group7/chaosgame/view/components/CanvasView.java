package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * The CanvasView class for the Chaos Game application.
 * <p>
 * This class is a custom {@link HBox} that contains an ImageView.
 * It is used to display the canvas of the Chaos Game application.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see HBox
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class CanvasView extends HBox {
  /**
   * The ImageView of the CanvasView.
   */
  private final ImageView imageView;

  /**
   * The constructor for the CanvasView.
   *
   * @param width the width of the CanvasView.
   * @param height the height of the CanvasView.
   */
  public CanvasView(int width, int height) {
    this.imageView = new ImageView();
    this.imageView.setFitWidth(width);
    this.imageView.setFitHeight(height);

    this.setMinSize(1, 1);
    imageView.fitWidthProperty().bind(this.widthProperty());
    imageView.fitHeightProperty().bind(this.heightProperty());
    this.getChildren().add(imageView);
  }

  /**
   * Returns the ImageView of the CanvasView.
   *
   * @return the ImageView of the CanvasView.
   */
  public ImageView getImageView() {
    return imageView;
  }
}
