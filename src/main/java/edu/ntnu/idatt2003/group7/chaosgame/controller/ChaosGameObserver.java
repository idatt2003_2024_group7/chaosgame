package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;

/**
 * Interface for the observer in the observer pattern.
 * <p>
 * The observer interface is used to update the observer when the subject changes.
 * </p>
 */
public interface ChaosGameObserver {

  /**
   * Updates the observer.
   *
   * @param chaosGame the chaos game object
   */
  void update(ChaosGame chaosGame);
}
