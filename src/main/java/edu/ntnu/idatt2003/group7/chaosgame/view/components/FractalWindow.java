package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * The FractalWindow class of the ChaosGame application.
 * <p>
 * The FractalWindow class is a VBox that contains the input fields for the fractal.
 * The fractal input fields are divided into two sections, affine and julia.
 * The affine section contains a table for affine transformations.
 * The julia section contains two text fields for the complex number c.
 * The fractal input fields also contain input fields for the minimum and maximum values
 * of the fractal,
 * the number of steps for the fractal and buttons for running and saving the fractal.
 * The FractalWindow also contains a notification area for displaying notifications.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see VBox
 * @author Ylva Björnerstedt, Helene Kjerstad
 */
public class FractalWindow extends VBox {
  /**
   * The minField is a TextField for the minimum value of the fractal.
   */
  private TextField minField;
  /**
   * The maxField is a TextField for the maximum value of the fractal.
   */
  private TextField maxField;
  /**
   * The stepsField is a TextField for the steps of the fractal.
   */
  private TextField stepsField;
  /**
   * The run is a Button for running the fractal.
   */
  private Button run;
  /**
   * The save is a Button for saving the fractal.
   */
  private Button save;
  /**
   * The addRow is a Button for adding a row to the affine table.
   */
  private Button addRow;
  /**
   * The juliaTextFields is a List of List of TextFields for the julia fractal.
   */
  List<List<TextField>> juliaTextFields;
  /**
   * The affineTextFields is a List of List of TextFields for the affine fractal.
   */
  List<List<TextField>> affineTextFields;
  /**
   * The julia is a VBox for the julia fractal input fields.
   */
  private VBox julia;
  /**
   * The affine is a VBox for the affine fractal input fields.
   */
  private VBox affine;
  /**
   * The affineTable is a GridPane for the affine fractal input fields.
   */
  private GridPane affineTable;
  /**
   * The notification is a Notification for the notification area.
   */
  private Notification notification;
  /**
   * The title is a Label for the title of the fractal.
   */
  private Label title;

  /**
   * Constructor for FractalWindow.
   */
  public FractalWindow() {
    makeTab();
  }

  /**
   * Method for making the tab.
   */
  private void makeTab() {
    this.getStyleClass().add("fractal-window-tab");
    this.getChildren().addAll(topRow(), body(), bottomRow());
    makeNotificationArea();
  }

  /**
   * Method for making the notification area.
   */
  private void makeNotificationArea() {
    notification = new Notification();
    notification.getStyleClass().add("notification-area");
  }

  /**
   * Method for making the top row.
   *
   * @return HBox
   */
  private HBox topRow() {
    HBox topRow = new HBox();
    topRow.getStyleClass().add("centered");
    title = new Label("Fractal name");
    title.getStyleClass().add("title-label");
    topRow.getChildren().add(title);
    return topRow;
  }

  /**
   * Method for making the body.
   *
   * @return VBox
   */
  private VBox body() {
    VBox body = new VBox();
    body.getStyleClass().add("row");

    // Make display for fractal fields
    StackPane display = new StackPane();
    display.getStyleClass().add("fractal-display");
    display.getChildren().addAll(affine(), julia());

    body.getChildren().addAll(display, minMaxCanvas(), steps());
    return body;
  }

  /**
   * Method for making the julia container.
   *
   * @return the julia container as a VBox
   */
  private VBox julia() {
    juliaTextFields = new ArrayList<>();
    julia = new VBox();
    julia.getStyleClass().add("row");

    VBox fieldsContainer = new VBox();
    fieldsContainer.getStyleClass().add("row");

    // Create labels and text fields for the transformation
    Label c;
    c = new Label("c = ");
    TextField a = new TextField();
    a.getStyleClass().add("big-textField");
    a.setPromptText("a");

    Label plus;
    plus = new Label(" + ");

    HBox vectorContainer = new HBox();
    vectorContainer.getStyleClass().add("row");
    TextField b = new TextField();
    b.getStyleClass().add("big-textField");
    b.setPromptText("b");
    Label i = new Label(" i");
    vectorContainer.getChildren().addAll(b, i);

    List<TextField> textFields = new ArrayList<>();
    textFields.add(a);
    textFields.add(b);
    juliaTextFields.add(textFields);
    fieldsContainer.getChildren().addAll(c, a, plus, vectorContainer);

    julia.getChildren().add(fieldsContainer);
    return julia;
  }

  /**
   * Method for making the affine container.
   *
   * @return VBox
   */
  private VBox affine() {
    affineTextFields = new ArrayList<>();
    affine = new VBox();
    affine.getStyleClass().add("row");

    ScrollPane affineScroll = new ScrollPane();
    affineScroll.getStyleClass().add("small-affine-scroll");
    VBox affineContainer = new VBox();
    affineContainer.getStyleClass().add("row");

    // Set up grid table for adding affine transformations
    affineTable = new GridPane();
    affineTable.getStyleClass().add("affine-table");

    // make header for table
    affineTableHeader();

    affineTable.setHgap(5);
    affineTable.setVgap(5);

    addRow = new Button("+ Row");
    addRow.getStyleClass().add("affine-long-add-row-button");
    HBox addRowContainer = new HBox();
    addRowContainer.getStyleClass().add("left");
    addRowContainer.getChildren().add(addRow);

    // Add all to continer
    affineContainer.getChildren().addAll(affineTable, addRowContainer);

    affineScroll.setContent(affineContainer);
    affine.getChildren().add(affineScroll);

    affine.getChildren().addAll();
    return affine;
  }

  /**
   * Method for making the affine table header.
   */
  public void affineTableHeader() {
    // Make label
    Label a = new Label("A:");
    a.getStyleClass().add("affine-label");
    Label b = new Label("b:");
    b.getStyleClass().add("affine-label");

    // Add labels, set spacing for gridpane and add 1.st row
    affineTable.add(a, 0, 1);
    affineTable.add(b, 1, 1);
  }

  /**
   * Method for adding an affine row.
   *
   * @param row the row index
   */
  public void addAffineRow(int row) {
    // Create textfields
    TextField a = new TextField();
    a.setPromptText("a00, a01, a10, a11");
    a.getStyleClass().add("small-affine-textField");
    TextField b = new TextField();
    b.setPromptText("x, y");
    b.getStyleClass().add("small-short-affine-textField");

    List<TextField> textFields = new ArrayList<>();
    textFields.add(a);
    textFields.add(b);
    affineTextFields.add(textFields);

    // Insert into gridpane
    affineTable.add(a, 0, row);
    affineTable.add(b, 1, row);
  }

  /**
   * Method for adding a remove affine row button.
   *
   * @param row the row index
   * @param remove the remove button
   */
  public void addRemoveAffineRow(int row, Button remove) {
    // Add row with remove button
    addAffineRow(row);
    affineTable.add(remove, 2, row);
  }

  /**
   * Returns the julia container.
   *
   * @return the julia container
   */
  public VBox getJulia() {
    return julia;
  }

  /**
   * Returns the affine container.
   *
   * @return the affine container
   */
  public VBox getAffine() {
    return affine;
  }

  /**
   * Method for making the min max canvas.
   *
   * @return returns the min max canvas as a HBox
   */
  private HBox minMaxCanvas() {
    HBox row = new HBox();
    row.getStyleClass().add("row");

    minField = new TextField();
    maxField = new TextField();

    minField.getStyleClass().add("small-textField");
    minField.setPromptText("x, y");
    maxField.getStyleClass().add("small-textField");
    maxField.setPromptText("x, y");

    Label min = new Label("Min:");
    Label max = new Label("Max:");
    row.getChildren().addAll(min, minField, max, maxField);
    HBox.setHgrow(minField, Priority.ALWAYS);
    HBox.setHgrow(maxField, Priority.ALWAYS);
    return row;
  }

  /**
   * Method for making the steps.
   *
   * @return returns the steps as a HBox
   */
  private HBox steps() {
    HBox row = new HBox();
    row.getStyleClass().add("row");
    Label stepsLabel = new Label("Steps:");
    stepsField = new TextField();
    row.getChildren().addAll(stepsLabel, stepsField);
    return row;
  }

  /**
   * Method for making the bottom row.
   *
   * @return the bottom row as a HBox
   */
  private HBox bottomRow() {
    HBox bottomRow = new HBox();
    bottomRow.getStyleClass().add("bottom-row");
    save = new Button("Save");
    run = new Button("Run");
    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);
    bottomRow.getChildren().addAll(save, spacer, run);
    return bottomRow;
  }

  /**
   * Adds a notification to the window.
   */
  public void addNotification() {
    this.setMaxHeight(620);
    this.setMinHeight(600);
    this.getChildren().add(notification);
  }

  /**
   * Removes a notification from the window.
   */
  public void removeNotification() {
    this.setMaxHeight(550);
    this.setMinHeight(550);
    this.getChildren().remove(notification);
  }

  /**
   * Returns the min field.
   *
   * @return the min field
   */
  public TextField getMinField() {
    return minField;
  }

  /**
   * Returns the max field.
   *
   * @return the max field
   */
  public TextField getMaxField() {
    return maxField;
  }

  /**
   * Returns the steps field.
   *
   * @return the steps field
   */
  public TextField getStepsField() {
    return stepsField;
  }

  /**
   * Returns the run button.
   *
   * @return the run button
   */
  public Button getRunButton() {
    return run;
  }

  /**
   * Returns the save button.
   *
   * @return the save button
   */
  public Button getSaveButton() {
    return save;
  }

  /**
   * Returns the julia fields.
   *
   * @return the julia fields
   */
  public List<List<TextField>> getJuliaFields() {
    return juliaTextFields;
  }

  /**
   * Returns the affine fields.
   *
   * @return the affine fields
   */
  public List<List<TextField>> getAffineFields() {
    return affineTextFields;
  }

  /**
   * Returns the add row button.
   *
   * @return the add row button
   */
  public Button getAddRowButton() {
    return addRow;
  }

  /**
   * Returns the affine table row count.
   *
   * @return the affine table row count
   */
  public int getRowCount() {
    return affineTable.getRowCount();
  }

  /**
   * Returns the affine table.
   *
   * @return the affine table
   */
  public GridPane getAffineTable() {
    return affineTable;
  }

  /**
   * Returns the notification box.
   *
   * @return the notification box
   */
  public Notification getNotificationBox() {
    return notification;
  }

  /**
   * Returns the title.
   *
   * @return the title
   */
  public Label getTitle() {
    return title;
  }
}
