package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ChaosGameController class is the controller for the chaos game.
 * <p>
 * The class provides methods to run and update the chaos game.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGameController {

  /**
   * The ChaosGame object.
   */
  private ChaosGame chaosGame;
  /**
   * The standard number of steps to run the chaos game.
   * This is used when the user does not specify the number of steps.
   */
  private static final int STEPS = 10000000;
  /**
   * The logger object.
   */
  private static final Logger logger = LoggerFactory.getLogger(ChaosGameController.class);
  /**
   * The boolean detailedRender. If true, the chaos game will run the modified Julia set algorithm.
   */
  private boolean detailedRender;
  /**
   * The ExecutorService for the chaos game.
   */
  private ExecutorService executorService;
  /**
   * The ExecutorService for the task.
   */
  private ExecutorService taskExecutor;
  /**
   * The Future object for the task.
   */
  private Future<?> taskFuture;
  /**
   * The list of Future objects for the chaos game calculations.
   */
  List<Future<Void>> futures;

  /**
   * Constructor for ChaosGameController,
   * creates a new ChaosGameController with the given width and height.
   *
   * @param width The width of the canvas.
   * @param height The height of the canvas.
   */
  public ChaosGameController(int width, int height) {
    setupChaosGame(ChaosGameDescriptionFactory.juliaDescription(), width, height);
    detailedRender = false;
    executorService = Executors.newFixedThreadPool(3);
    taskExecutor = Executors.newSingleThreadExecutor();
  }

  /**
   * The method sets up the chaos game with the given description, width and height,
   * and runs the chaos game.
   */
  private void setupChaosGame(ChaosGameDescription description, int width, int height) {
    try {
      chaosGame = new ChaosGame(description, width, height);
    } catch (Exception e) {
      logger.atError().log("Error occurred when creating ChaosGame object: " + e.getMessage());
    }
  }

  /**
   * The method updates the chaos game with the given description.
   *
   * @param description the new description of the chaos game.
   */
  public void updateChaosGame(ChaosGameDescription description) {
    try {
      chaosGame.updateChaosGameDescription(description);
    } catch (Exception e) {
      logger.atError().log("Error occurred when updating ChaosGameDescription: " + e.getMessage());
    }
  }

  /**
   * The method loads the chaos game with the given description,
   * updates the chaos game and runs the chaos game.
   *
   * @param description the new description of the chaos game.
   */
  public void loadChaosGame(ChaosGameDescription description) {
    try {
      chaosGame.updateChaosGameDescription(description);
      runChaosGame();
    } catch (Exception e) {
      logger.atError().log("Error occurred when updating ChaosGameDescription: " + e.getMessage());
    }
  }

  /**
   * The method returns the chaos game.
   *
   * @return the chaos game.
   */
  public ChaosGame getChaosGame() {
    return chaosGame;
  }

  /**
   * The method runs the chaos game with the given number of steps.
   * <p>
   * The method uses a Task object to run the chaos game in a thread separate from
   * the JavaFX thread.
   * To make the application remain responsive when calculating the chaos game.
   * The task is submitted to a single thread executor. and uses a ExecutorService to run the
   * chaos game in separate threads. The task waits on all threads to finish before
   * notifying the observers in the javafx thread.
   * </p>
   *
   * @param steps the number of steps to run the chaos game.
   */
  public void runChaosGame(int steps) {
    cleanUp();
    taskExecutor = Executors.newSingleThreadExecutor();
    Task<Void> task = new Task<>() {
      @Override
      protected Void call() {
        executorService = Executors.newFixedThreadPool(3);
        if (detailedRender && chaosGame.getDescription().isJulia()) {
          futures = chaosGame.runModifiedJulia(steps, executorService);
        } else {
          futures = chaosGame.runSteps(steps, executorService);
        }
        executorService.shutdown(); // do not accept new tasks
        while (!executorService.isTerminated()) {
          // wait for all tasks to finish
        }
        return null;
      }

      @Override
      protected void succeeded() {
        // notify observers when all tasks are finished in the javafx thread
        chaosGame.notifyObservers();
      }
    };
    taskFuture = taskExecutor.submit(task);
    taskExecutor.shutdown(); // do not accept new tasks
  }

  /**
   * The method runs the chaos game with the standard number of steps.
   */
  private void runChaosGame() {
    if (detailedRender && chaosGame.getDescription().isJulia()) {
      runChaosGame(STEPS / 100);
    } else {
      runChaosGame(STEPS);
    }
  }

  /**
   * Cleans up the chaos game threads to try to avoid not needed threads
   * running in the background.
   * The method cancels the futures and shuts down the executor services.
   */
  public void cleanUp() {
    try {
      if (futures != null) {
        futures.forEach(future -> future.cancel(true));
      }
      if (taskFuture != null) {
        taskFuture.cancel(true);
      }
    } catch (Exception e) {
      logger.atError().log("Error occurred when canceling futures: " + e.getMessage());
    }
    try {
      if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
        executorService.shutdownNow();
      }
      if (!taskExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
        taskExecutor.shutdownNow();
      }
    } catch (InterruptedException e) {
      executorService.shutdownNow();
      taskExecutor.shutdownNow();
      Thread.currentThread().interrupt();
      logger.atError().log("Error occurred when awaiting termination: " + e.getMessage());
    }
  }

  /**
   * Sets the boolean detailedRender.
   * If true, the chaos game will run the modified Julia set algorithm.
   *
   * @param detailedRender the boolean to set.
   */
  public void setDetailedRender(boolean detailedRender) {
    this.detailedRender = detailedRender;
  }

  /**
   * The method adds an observer to the chaos game.
   *
   * @param observer the observer to add.
   */
  public void addChaosGameObserver(ChaosGameObserver observer) {
    chaosGame.addObserver(observer);
  }

  /**
   * The method updates the canvas size of the chaos game and runs the chaos game.
   *
   * @param size the new size of the canvas.
   */
  public void updateCanvasSize(Vector2D size) {
    try {
      chaosGame.updateCanvasSize(size);
      runChaosGame();
    } catch (IllegalArgumentException e) {
      logger.atError().log("Error occurred when updating canvas size: " + e.getMessage());
    }
  }
}
