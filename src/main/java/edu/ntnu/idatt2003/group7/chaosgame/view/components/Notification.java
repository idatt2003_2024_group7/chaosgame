package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * The Notification class for the Chaos Game application.
 * <p>
 * This class is a custom {@link VBox} that contains a list of messages to be displayed to the user.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @see VBox
 * @author Helene Kjerstad, Ylva Björnerstedt
 */

public class Notification extends VBox {
  /**
   * The list of messages to be displayed.
   */
  private final List<String> messages;

  /**
   * The constructor for the Notification.
   */
  public Notification() {
    super();
    this.getStyleClass().add("notification-area");
    messages = new ArrayList<>();
  }

  /**
   * Adds a message to the list of messages to be displayed.
   * Message will not be added if it is already in the list or the list is full.
   *
   * @param text the message to be displayed.
   */
  public void add(String text) {
    // Only add the message if it is not already in the list and the list is not full
    if (messages.size() == 2 || messages.contains(text)) {
      return;
    }
    messages.add(text);
    Label label = new Label(text);
    label.setWrapText(true);
    super.getChildren().add(label);
  }

  /**
   * Clears the list of messages to be displayed and
   * removes all messages from the Notification area.
   *
   */
  public void clear() {
    messages.clear();
    super.getChildren().clear();
  }

  /**
   * Returns whether the list of messages is empty or not.
   *
   * @return true if the list of messages is empty, false otherwise.
   */
  public Boolean isEmpty() {
    return super.getChildren().isEmpty();
  }
}
