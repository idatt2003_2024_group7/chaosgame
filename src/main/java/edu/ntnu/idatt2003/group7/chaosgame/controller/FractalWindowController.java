package edu.ntnu.idatt2003.group7.chaosgame.controller;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import edu.ntnu.idatt2003.group7.chaosgame.view.components.FractalWindow;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The FractalWindowController class is the controller for the fractal window.
 * <p>
 * The class provides methods to control the fractal window, which is used to edit the chaos game.
 * </p>
 *
 * @version 1.0
 * @since 0.3
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class FractalWindowController implements ChaosGameObserver {
  /**
   * The FractalWindow object.
   */
  private final FractalWindow fractalWindow;
  /**
   * The consumer to update the chaos game.
   */
  private final Consumer<ChaosGameDescription> updateChaosGame;
  /**
   * The consumer to run the chaos game.
   */
  private final Consumer<Integer> runChaosGame;
  /**
   * The consumer to save to file.
   */
  private final Consumer<ChaosGameDescription> saveToFileChooser;
  /**
   * The boolean value that determines if the fractal window is visible.
   */
  private Boolean visible = false;
  /**
   * The button to display the fractal window.
   */
  private Button fractalButton;
  /**
   * The ChaosGameDescription object.
   */
  private ChaosGameDescription chaosGameDescription;
  /**
   * The logger object.
   */
  private static final Logger logger = LoggerFactory.getLogger(FractalWindowController.class);
  /**
   * The maximum number of steps to run the chaos game.
   */
  private static final int MAX_STEPS = 100000000;

  /**
   * Constructor for FractalWindowController,
   * creates a new controller for the FractalWindow and sets up the window.
   *
   * @param fractalWindow The FractalWindow object.
   * @param updateChaosGame The consumer to update the chaos game.
   * @param runChaosGame The consumer to run the chaos game.
   * @param saveToFileChooser The consumer to save to file.
   */
  public FractalWindowController(FractalWindow fractalWindow,
                                 Consumer<ChaosGameDescription> updateChaosGame,
                                 Consumer<Integer> runChaosGame,
                                 Consumer<ChaosGameDescription> saveToFileChooser) {
    this.fractalWindow = fractalWindow;
    this.updateChaosGame = updateChaosGame;
    this.runChaosGame = runChaosGame;
    this.saveToFileChooser = saveToFileChooser;
    setup();
  }

  /**
   * Sets up the fractal window.
   * <p>
   *   The method sets up the fractal window by setting the run button, save button, add row button,
   *   affine table.
   *   The method sets the visibility of the fractal window to false.
   * </p>
   */
  private void setup() {
    setRunButton();
    setSaveButton();
    setAddRowButton();
    setAffineTable();
    displayAffine();
    fractalWindow.setVisible(false);
  }

  /**
   * Sets the run button.
   * <p>
   *   The method sets the action for the run button.
   *   The action is to run the chaos game.
   * </p>
   */
  private void setRunButton() {
    fractalWindow.getRunButton().setOnAction(e -> runChaosGame());
  }

  /**
   * Sets the save button.
   * <p>
   *   The method sets the action for the save button.
   *   The action is to save the chaos game to a file
   * </p>
   */
  private void setSaveButton() {
    fractalWindow.getSaveButton().setOnAction(e -> save());
  }

  /**
   * Displays the fractal window.
   * <p>
   *   The method sets the visibility of the fractal window to the opposite
   *   of the current visibility.
   *   If the fractal window is visible, the method
   *   adds the active-fractal-button style class to the button.
   *   If the fractal window is not visible, the method removes the
   *   active-fractal-button style class from the button.
   * </p>
   *
   */
  private void display() {
    visible = !visible;
    fractalWindow.setVisible(visible);
    if (visible) {
      fractalButton.getStyleClass().add("active-fractal-button");
    } else {
      fractalButton.getStyleClass().remove("active-fractal-button");
    }
  }

  /**
   * Saves the chaos game to a file.
   * <p>
   *   The method saves the chaos game to a file by calling the saveToFileChooser
   *   consumer with the chaos game description.
   * </p>
   */
  private void save() {
    try {
      saveToFileChooser.accept(makeDescription());
    } catch (Exception e) {
      setNotification("Could not save to file : Please make sure all input is valid");
    }
  }

  /**
   * Runs the chaos game.
   * <p>
   *   The method runs the chaos game by calling the makeDescription method
   *   to get the chaos game description. The method then updates the chaos game
   *   with the description, and runs the chaos game with the given amount of steps.
   * </p>
   */
  private void runChaosGame() {
    fractalWindow.getNotificationBox().clear();
    fractalWindow.removeNotification();
    try {
      chaosGameDescription = makeDescription();
    } catch (Exception e) {
      logger.atError().log("Error occurred when creating ChaosGameDescription: " + e.getMessage());
      return;
    }
    int steps;
    try {
      steps = Utility.parseStepsTextField(fractalWindow.getStepsField());
      if (steps > MAX_STEPS) {
        throw new IllegalArgumentException("The number of steps must be less than " + MAX_STEPS);
      }
      updateChaosGame.accept(chaosGameDescription);
      runChaosGame.accept(steps);
      display();
    } catch (Exception e) {
      setNotification(e.getMessage());
    }
  }

  /**
   * Sets the notification.
   * <p>
   *   The method sets the notification with the given message.
   *   If the notification box is empty, the method adds a notification box.
   *   The method then adds the message to the notification box.
   * </p>
   *
   * @param message The message.
   */
  private void setNotification(String message) {
    if (fractalWindow.getNotificationBox().isEmpty()) {
      fractalWindow.addNotification();
    }
    fractalWindow.getNotificationBox().add(message);
  }

  /**
   * Updates the fields in the fractal window.
   * <p>
   *   The method updates the fields in the fractal window with the given
   *   chaos game description. The method sets the min and max fields, and checks
   *   if the chaos game description is a julia or affine. Then the method updates
   *   the fields accordingly.
   * </p>
   *
   * @param chaosGameDescription The chaos game description.
   */
  private void updateFields(ChaosGameDescription chaosGameDescription) {
    this.chaosGameDescription = chaosGameDescription;
    fractalWindow.getNotificationBox().clear();
    fractalWindow.removeNotification();

    // Set the min, max fields
    fractalWindow.getMinField().setText(chaosGameDescription.getMinCoords().getX0()
        + ", " + chaosGameDescription.getMinCoords().getX1());
    fractalWindow.getMaxField().setText(chaosGameDescription.getMaxCoords().getX0()
        + ", " + chaosGameDescription.getMaxCoords().getX1());

    // Check if julia or affine and update the fields
    List<Transform2D> transforms = chaosGameDescription.getTransforms();
    if (transforms.getFirst() instanceof JuliaTransform) {
      updateJulia(transforms);
      displayJulia();
    } else {
      updateAffine(transforms);
      displayAffine();
    }
  }

  /**
   * Updates the julia fields.
   * <p>
   *   The method updates the julia fields with the given list of transforms.
   *   The method reads the fields from the fractal window, and updates the fields
   *   with the values from the transforms.
   * </p>
   *
   * @param transforms The list of transforms.
   */
  private void updateJulia(List<Transform2D> transforms) {
    JuliaTransform juliaTransform = (JuliaTransform) transforms.getFirst();
    List<List<TextField>> textFields = fractalWindow.getJuliaFields();
    // Update the fields
    textFields.getFirst().get(0).setText(String.valueOf((juliaTransform.getPoint().getX0())));
    textFields.getFirst().get(1).setText(String.valueOf((juliaTransform.getPoint().getX1())));
    setTitle("Julia Fractal");
  }

  /**
   * Updates the affine fields.
   * <p>
   *   The method updates the affine fields with the given list of transforms.
   *   The method reads the fields from the fractal window, and updates the fields
   *   with the values from the transforms. Adding or removing rows if necessary.
   * </p>
   *
   * @param transforms The list of transforms.
   */
  private void updateAffine(List<Transform2D> transforms) {
    List<List<TextField>> textFields = fractalWindow.getAffineFields();
    // Make sure there are enough rows in the table
    if (textFields.size() != transforms.size()) {
      // clear table and text fields
      fractalWindow.getAffineFields().clear();
      clearAffineTable();
      fractalWindow.affineTableHeader();
      setAffineTable();
      // add new rows and text fields
      for (int i = 1; i < transforms.size(); i++) {
        addAffineRow();
      }
      // Update the textFields after changing the table
      textFields = fractalWindow.getAffineFields();
    }
    // Update the fields
    for (int i = 0; i < transforms.size(); i++) {
      AffineTransform2D affineTransform = (AffineTransform2D) transforms.get(i);
      textFields.get(i).get(0).setText(affineTransform.getMatrix().toString());
      textFields.get(i).get(1).setText(affineTransform.getVector().getX0()
          + ", " + affineTransform.getVector().getX1());
    }
    setTitle("Affine Fractal");
  }

  /**
   * Makes a chaos game description.
   * <p>
   *   The method makes a chaos game description by reading the fields from the fractal
   *   window. The method reads the min and max fields, and checks if the chaos game
   *   description is a julia or affine. If the min and max fields are empty,
   *   the method sets the min and max coords to the default values of -1 and 1.
   *   Then the method reads the fields accordingly and makes a game description.
   * </p>
   *
   * @return The chaos game description.
   */
  private ChaosGameDescription makeDescription() {
    Vector2D min = null;
    Vector2D max = null;

    // Read fields from the fractal window
    try {
      min = Utility.parseVector(fractalWindow.getMinField().getText());
      max = Utility.parseVector(fractalWindow.getMaxField().getText());
    } catch (Exception e) {
      setNotification("Invalid input for min/max values: " + e.getMessage());
    }

    // Check if julia or affine and read the fields
    List<Transform2D> transforms;
    List<List<TextField>> textFields;
    if (chaosGameDescription.getTransforms().getFirst() instanceof JuliaTransform) {

      // Read julia fields
      textFields = fractalWindow.getJuliaFields();
      transforms = readJulia(textFields);
      setTitle("Julia Fractal");
    } else {

      // Read affine fields
      textFields = fractalWindow.getAffineFields();
      transforms = readAffine(textFields);
      setTitle("Affine Fractal");
    }

    chaosGameDescription = new ChaosGameDescription(transforms, min, max);
    return chaosGameDescription;
  }

  /**
   * Reads the julia fields.
   * <p>
   *   The method reads the julia fields with the given list of text fields.
   *   The method reads the fields from the text fields, and makes a list of transformations.
   *   If any of the fields are empty, the method returns an empty list.
   * </p>
   *
   * @param textFields The list of text fields.
   * @return The list of transformations.
   */
  private List<Transform2D> readJulia(List<List<TextField>> textFields) {
    List<Transform2D> transforms = new ArrayList<>();
    double real = 0;
    double imaginary = 0;
    int count = 0;

    // Read all text fields
    try {
      for (List<TextField> row : textFields) {
        for (TextField textField : row) {
          if (count == 0) {
            real = Utility.parseDoubleTextField(textField);
            count++;
          } else {
            imaginary = Utility.parseDoubleTextField(textField);
            count = 0;
          }
        }
      }
      // Make list of transformations
      Complex point = new Complex(real, imaginary);
      transforms.add(new JuliaTransform(point, 1));
      transforms.add(new JuliaTransform(point, -1));

    } catch (NumberFormatException e) {
      setNotification("Invalid input for transformation: Please enter a number");
    } catch (IndexOutOfBoundsException e) {
      setNotification("Invalid input for transformation: " + e.getMessage());
      transforms = new ArrayList<>();
    } catch (Exception e) {
      logger.atError().log("Error occurred when reading Julia textFields: " + e.getMessage());
      transforms = new ArrayList<>();
    }
    return transforms;
  }

  /**
   * Reads the affine fields.
   * <p>
   *   The method reads the affine fields with the given list of text fields.
   *   The method reads the fields from the text fields, and makes a list of transformations.
   *   If any of the fields are empty, the method returns an empty list.
   * </p>
   *
   * @param textFields The list of text fields.
   * @return The list of transformations.
   */
  private List<Transform2D> readAffine(List<List<TextField>> textFields) {
    List<Transform2D> transforms = new ArrayList<>();
    int count;
    Matrix2x2 matrix = null;
    Vector2D vector = null;

    // Read all text fields
    for (List<TextField> row : textFields) {
      count = 0;
      for (TextField textField : row) {
        try {
          if (count == 0) {
            // Matrix
            matrix = Utility.parseMatrix(textField.getText());
            count++;
          } else {
            // x and y values
            vector = Utility.parseVector(textField.getText());
          }
        } catch (NumberFormatException e) {
          setNotification("Invalid input for transformation: Please enter a number");
          transforms = new ArrayList<>();
        } catch (IllegalArgumentException e) {
          setNotification(e.getMessage());
          transforms = new ArrayList<>();
        } catch (Exception e) {
          logger.atError().log("Error occurred when reading Affine textFields: " + e.getMessage());
          transforms = new ArrayList<>();
        }
      }
      // add new affine transform
      transforms.add(new AffineTransform2D(matrix, vector));
    }
    return transforms;
  }

  /**
   * Adds an affine row.
   * <p>
   *   The method adds an affine row to the affine table.
   *   The method adds a button to remove the row, and adds the row to the table.
   * </p>
   */
  private void addAffineRow() {
    int row = fractalWindow.getRowCount();

    // Add button to remove row
    Button remove = new Button("X");
    remove.setOnAction(event ->
        fractalWindow.getAffineTable().getChildren()
            .removeIf(node -> GridPane.getRowIndex(node) == row));
    remove.getStyleClass().add("remove-button");

    // Add row to table
    fractalWindow.addRemoveAffineRow(row, remove);
  }

  /**
   * Sets the add row button.
   * <p>
   *   The method sets the action for the add row button.
   *   The action is to add an affine row.
   * </p>
   */
  private void setAddRowButton() {
    fractalWindow.getAddRowButton().setOnAction(e -> addAffineRow());
  }

  /**
   * Displays the julia.
   * <p>
   *   The method sets the visibility of the julia to true, and the affine to false.
   * </p>
   */
  private void displayJulia() {
    fractalWindow.getJulia().setVisible(true);
    fractalWindow.getAffine().setVisible(false);
  }

  /**
   * Displays the affine.
   * <p>
   *   The method sets the visibility of the affine to true, and the julia to false.
   * </p>
   */
  private void displayAffine() {
    fractalWindow.getJulia().setVisible(false);
    fractalWindow.getAffine().setVisible(true);

  }

  /**
   * Clears the affine table.
   * <p>
   *   The method clears the affine table by removing all children from the table.
   * </p>
   */
  private void clearAffineTable() {
    fractalWindow.getAffineTable().getChildren().clear();
  }

  /**
   * Sets the title of the fractal window.
   *
   * @param title The title.
   */
  private void setTitle(String title) {
    fractalWindow.getTitle().setText(title);
  }

  /**
   * Sets the fractal window button.
   *
   * @param fractalButton The fractal button.
   */
  public void setFractalButton(Button fractalButton) {
    this.fractalButton = fractalButton;
    fractalButton.setOnAction(e -> display());
  }

  /**
   * Sets the affine table.
   * <p>
   *   The method sets the affine table by adding the first row with text fields.
   * </p>
   */
  public void setAffineTable() {
    fractalWindow.addAffineRow(fractalWindow.getRowCount());
  }

  /**
   * Updates the chaos game.
   * <p>
   *   The method updates the chaos game with the given chaos game.
   *   This is used for the observer pattern.
   * </p>
   *
   * @param chaosGame The chaos game.
   */
  @Override
  public void update(ChaosGame chaosGame) {
    updateFields(chaosGame.getDescription());
  }
}
