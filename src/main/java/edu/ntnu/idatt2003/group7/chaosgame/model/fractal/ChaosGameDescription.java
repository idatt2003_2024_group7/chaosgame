package edu.ntnu.idatt2003.group7.chaosgame.model.fractal;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import java.util.List;

/**
 * The ChaosGameDescription class represents a description of a chaos game.
 * <p>
 * The class provides methods to get the transforms, minCoords and maxCoords.
 * </p>
 *
 * @version 1.0
 * @since 0.2
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGameDescription {
  /**
   * The min coordinates.
   */
  private final Vector2D minCoords;
  /**
   * The max coordinates.
   */
  private final Vector2D maxCoords;
  /**
   * The list of transforms.
   */
  private final List<Transform2D> transforms;

  /**
   * Constructor for ChaosGameDescription.
   *
   * @param transforms a list of Transform2D objects
   * @param minCoords the min coordinate
   * @param maxCoords the max coordinate
   * @throws IllegalArgumentException if: <ul>
   *                      <li>minCoords is null</li>
   *                      <li>maxCoords is null</li>
   *                      <li>transforms is null</li>
   *                      <li>transforms is empty</li>
   *                      </ul>
   */
  public ChaosGameDescription(List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    try {
      validateVector(minCoords, "minCoords");
      validateVector(maxCoords, "maxCoords");
      validateCoords(minCoords, maxCoords);
      validateTransform2D(transforms);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create ChaosGameDescription object. "
          + e.getMessage());
    }

    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /**
   * Validates that the vector is a valid 2D vector.
   *
   * @param vector the vector to be validated
   * @param name the name of the parameter
   *
   * @throws IllegalArgumentException if either of the parameters are null.
   */
  private void validateVector(Vector2D vector, String name) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException(String.format("The %s cannot be null", name));
    }
  }

  /**
   * Validates the given list of Transform2D objects.
   *
   * @param transforms the list of Transform2D objects to be validated
   * @throws IllegalArgumentException if transforms is null or the list is empty
   */
  private void validateTransform2D(List<Transform2D> transforms) throws IllegalArgumentException {
    if (transforms == null) {
      throw new IllegalArgumentException("The list cannot be null");
    } else if (transforms.isEmpty()) {
      throw new IllegalArgumentException("The list cannot be empty");
    }
  }

  /**
   * Validates the coordinates.
   *
   * @param minCoords the min coordinates
   * @param maxCoords the max coordinates
   * @throws IllegalArgumentException <ul>
   *      <li>if the minCoords is greater than or equal to the maxCoords</li>
   *      <li>if the point (0,0) is not within the minCoords and maxCoords</li>
   *      </ul>
   */
  private void validateCoords(Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    if (minCoords.getX0() >= maxCoords.getX0() || minCoords.getX1() >= maxCoords.getX1()) {
      throw new IllegalArgumentException("The minCoords must be less than the maxCoords");
    }
    if (!(0 >= minCoords.getX0() && 0 <= maxCoords.getX0() && 0 >= minCoords.getX1()
        && 0 <= maxCoords.getX1())) {
      throw new IllegalArgumentException(
          "The point (0,0) must be within the minCoords and maxCoords");
    }
  }

  /**
   * Gets the transforms list.
   *
   * @return the list of Transform2D
   */
  public List<Transform2D> getTransforms() {
    return this.transforms;
  }

  /**
   * Gets the minCoords.
   *
   * @return the minCoords as an object of Vector2D
   */
  public Vector2D getMinCoords() {
    return this.minCoords;
  }

  /**
   * Gets the maxCoords.
   *
   * @return the maxCoords as an object of Vector2D
   */
  public Vector2D getMaxCoords() {
    return this.maxCoords;
  }

  /**
   * Checks if the first transform is a JuliaTransform.
   *
   * @return true if the first transform is a JuliaTransform
   */
  public boolean isJulia() {
    return transforms.getFirst() instanceof JuliaTransform;
  }

  /**
   * Returns a string representation of the object.
   *
   * @return a string representation of the object
   */
  @Override
  public String toString() {
    int indent = 40;
    StringBuilder sb = new StringBuilder();
    sb.append(Utility.writeLine(
        transforms.getFirst().getClass().getSimpleName().replace("Transform", ""),
        "Type of transform", indent)).append("\n");
    sb.append(Utility.writeLine(minCoords.toString(), "Lower left", indent)).append("\n");
    sb.append(Utility.writeLine(maxCoords.toString(), "Upper right", indent)).append("\n");

    if (transforms.getFirst() instanceof AffineTransform2D) {
      for (int i = 0; i < transforms.size(); i++) {
        sb.append(Utility.writeLine(transforms.get(i).toString(),
            i + 1 + ". transform", indent)).append("\n");
      }
    } else if (transforms.getFirst() instanceof JuliaTransform) {
      sb.append(Utility.writeLine(transforms.getFirst().toString(),
          "Real and imaginary parts of the constant c", indent)).append("\n");
    }
    return sb.toString();
  }
}
