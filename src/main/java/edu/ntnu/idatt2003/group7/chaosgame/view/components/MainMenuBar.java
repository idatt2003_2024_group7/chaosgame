package edu.ntnu.idatt2003.group7.chaosgame.view.components;

import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * The MainMenuBar class for the Chaos Game application.
 * <p>
 * This class is a custom {@link MenuBar} that contains the menu items for the main menu bar
 * of the Chaos Game application.
 * The menu bar contains the following menus:
 * </p>
 * <ul>
 *   <li>Create New Fractal</li>
 *   <li>Load Fractal</li>
 *   <li>Save Fractal</li>
 *   <li>Settings</li>
 * </ul>
 *
 * @version 1.0
 * @since 0.3
 * @see MenuBar
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class MainMenuBar extends MenuBar {
  /**
   * The new fractal menu.
   */
  private Menu newFractalMenu;
  /**
   * The new fractal menu item.
   */
  private MenuItem newFractalMenuItem;
  /**
   * The pre-defined fractals menu.
   */
  private Menu preDefinedFractalsMenu;
  /**
   * The Sierpinski triangle menu item.
   */
  private MenuItem sierpinskiTriangleMenuItem;
  /**
   * The Barnsley fern menu item.
   */
  private MenuItem barnsleyFernMenuItem;
  /**
   * The Julia set menu item.
   */
  private MenuItem juliaSetMenuItem;
  /**
   * The load fractal menu.
   */
  private Menu loadFractalMenu;
  /**
   * The load from path menu item.
   */
  private MenuItem loadFromPathMenuItem;
  /**
   * The load from folder menu.
   */
  private Menu loadFromFolderMenu;
  /**
   * The save fractal menu.
   */
  private Menu saveFractalMenu;
  /**
   * The save to path menu item.
   */
  private MenuItem saveToPathMenuItem;
  /**
   * The save to folder menu item.
   */
  private MenuItem saveToFolderMenuItem;
  /**
   * The save as image menu item.
   */
  private MenuItem saveAsImageMenuItem;
  /**
   * The settings menu.
   */
  private Menu settingsMenu;
  /**
   * The render color menu item.
   */
  private CustomMenuItem renderColorMenuItem;
  /**
   * The canvas color menu item.
   */
  private CustomMenuItem canvasColorMenuItem;
  /**
   * The render details menu item.
   */
  private CustomMenuItem renderDetailsMenuItem;

  /**
   * Constructs a new MainMenuBar.
   */
  public MainMenuBar() {
    make();
  }

  /**
   * Makes the menu bar.
   */
  private void make() {
    makeFractalMenu();
    makeLoadFractalMenu();
    makeSaveFractalMenu();
    makeSettingsMenu();

    this.getMenus().addAll(newFractalMenu, loadFractalMenu, saveFractalMenu, settingsMenu);
  }

  /**
   * Makes the fractal menu.
   */
  private void makeFractalMenu() {
    newFractalMenu = new Menu("Create New Fractal");
    newFractalMenuItem = new MenuItem("New fractal");

    preDefinedFractalsMenu = new Menu("Pre-defined fractals");
    sierpinskiTriangleMenuItem = new MenuItem("Sierpinski triangle");
    barnsleyFernMenuItem = new MenuItem("Barnsley fern");
    juliaSetMenuItem = new MenuItem("Julia set");
    preDefinedFractalsMenu.getItems().addAll(
        sierpinskiTriangleMenuItem, barnsleyFernMenuItem, juliaSetMenuItem);

    newFractalMenu.getItems().addAll(newFractalMenuItem, preDefinedFractalsMenu);
  }

  /**
   * Makes the load fractal menu.
   */
  private void makeLoadFractalMenu() {
    loadFractalMenu = new Menu("Load fractal");
    loadFromPathMenuItem = new MenuItem("Load from path");
    loadFromFolderMenu = new Menu("Load from folder");

    loadFractalMenu.getItems().addAll(loadFromPathMenuItem, loadFromFolderMenu);
  }

  /**
   * Makes the save fractal menu.
   */
  private void makeSaveFractalMenu() {
    saveFractalMenu = new Menu("Save fractal");

    saveToPathMenuItem = new MenuItem("Save to path");
    saveToFolderMenuItem = new MenuItem("Save to folder");
    saveAsImageMenuItem = new MenuItem("Save as image");

    saveFractalMenu.getItems().addAll(
        saveToPathMenuItem, saveToFolderMenuItem, saveAsImageMenuItem);
  }

  /**
   * Makes the settings menu.
   */
  private void makeSettingsMenu() {
    settingsMenu = new Menu("Settings");

    renderColorMenuItem = new CustomMenuItem();
    renderColorMenuItem.setHideOnClick(false);
    canvasColorMenuItem = new CustomMenuItem();
    canvasColorMenuItem.setHideOnClick(false);
    renderDetailsMenuItem = new CustomMenuItem();
    renderDetailsMenuItem.setHideOnClick(false);

    settingsMenu.getItems().addAll(renderColorMenuItem, canvasColorMenuItem, renderDetailsMenuItem);
  }

  /**
   * Returns the new fractal menu.
   *
   * @return the new fractal menu.
   */
  public MenuItem getNewFractalMenuItem() {
    return newFractalMenuItem;
  }

  /**
   * Returns the pre-defined fractals menu.
   *
   * @return the pre-defined fractals menu.
   */
  public Menu getPreDefinedFractalsMenu() {
    return preDefinedFractalsMenu;
  }

  /**
   * Returns the Sierpinski triangle menu item.
   *
   * @return the Sierpinski triangle menu item.
   */
  public MenuItem getSierpinskiTriangleMenuItem() {
    return sierpinskiTriangleMenuItem;
  }

  /**
   * Returns the Barnsley fern menu item.
   *
   * @return the Barnsley fern menu item.
   */
  public MenuItem getBarnsleyFernMenuItem() {
    return barnsleyFernMenuItem;
  }

  /**
   * Returns the Julia set menu item.
   *
   * @return the Julia set menu item.
   */
  public MenuItem getJuliaSetMenuItem() {
    return juliaSetMenuItem;
  }

  /**
   * Returns the load from path menu item.
   *
   * @return the load from path menu item.
   */
  public MenuItem getLoadFromPathMenuItem() {
    return loadFromPathMenuItem;
  }

  /**
   * Returns the load from folder menu.
   *
   * @return the load from folder menu.
   */
  public Menu getLoadFromFolderMenu() {
    return loadFromFolderMenu;
  }

  /**
   * Returns the save to path menu item.
   *
   * @return the save to path menu item.
   */
  public MenuItem getSaveToPathMenuItem() {
    return saveToPathMenuItem;
  }

  /**
   * Returns the save to folder menu item.
   *
   * @return the save to folder menu item.
   */
  public MenuItem getSaveToFolderMenuItem() {
    return saveToFolderMenuItem;
  }

  /**
   * Returns the save as image menu item.
   *
   * @return the save as image menu item.
   */
  public MenuItem getSaveAsImage() {
    return saveAsImageMenuItem;
  }

  /**
   * Returns the settings menu.
   *
   * @return the settings menu.
   */
  public Menu getSettingsMenu() {
    return settingsMenu;
  }

  /**
   * Returns the render color menu item.
   *
   * @return the render color menu item.
   */
  public CustomMenuItem getRenderColorMenuItem() {
    return renderColorMenuItem;
  }

  /**
   * Returns the canvas color menu item.
   *
   * @return the canvas color menu item.
   */
  public CustomMenuItem getCanvasColorMenuItem() {
    return canvasColorMenuItem;
  }

  /**
   * Returns the render details menu item.
   *
   * @return the render details menu item.
   */
  public CustomMenuItem getRenderDetailsMenuItem() {
    return renderDetailsMenuItem;
  }
}
