package edu.ntnu.idatt2003.group7.chaosgame.controller.cli;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.io.ChaosGameFileHandler;
import edu.ntnu.idatt2003.group7.chaosgame.utility.Utility;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * The ChaosGameCLI class is the command line interface for the Chaos Game application.
 * <p>
 * The class provides methods to interact with the user through the command line.
 * This class is intended to represent the main functionality
 * of the application, but not the final product.
 * Provides the following functionalities:
 * </p>
 *    <ul> <li>Read data from file</li>
 *    <li>Write to file</li>
 *    <li>Run steps</li>
 *    <li>Print fractals to console</li>
 *    <li>Exit</li> </ul>
 *
 * @version 1.0
 * @since 0.2
 * @author Helene Kjerstad, Ylva Björnerstedt
 */
public class ChaosGameCLI {
  /**
   * The file path used for reading and writing to file.
   */
  private String filePath;
  /**
   * The height of the ChaosGameCLI object.
   */
  private static final int HEIGHT = 40;
  /**
   * The width of the ChaosGameCLI object.
   */
  private static final int WIDTH = 100;
  /**
   * The ChaosGameFileHandler object.
   */
  private ChaosGameFileHandler fileHandler;
  /**
   * The ChaosGameDescription object.
   */
  private ChaosGameDescription chaosGameDescription;
  /**
   * The ChaosGame object.
   */
  private ChaosGame chaosGame;

  /**
   * Constructor for ChaosGameCLI.
   */
  public ChaosGameCLI() {
    fileHandler = new ChaosGameFileHandler();
  }

  /**
   * The main menu of the Chaos Game application.
   * <p>
   *   The method prints the main menu options and gets user input.
   *   The method calls the corresponding method based on the user input.
   *   The menu will loop until the user chooses to exit.
   * </p>
   */
  private void mainMenu() {
    System.out.println("Main menu");
    String [] options = {"Read data from file",
        "Write to file", "Run steps", "Print fractals to console", "Exit"};

    // Print menu options
    for (int i = 0; i < options.length; i++) {
      System.out.println(i + 1 + ". " + options[i]);
    }
    // Get user input
    Scanner scanner = new Scanner(System.in);
    int ans = Utility.getNewIntRange(scanner, options.length);

    switch (ans) {
      case 1:
        try {
          readFromFile();
        } catch (IOException e) {
          System.out.println("Error: " + e.getMessage());
        }
        break;
      case 2:
        writeToFile();
        break;
      case 3:
        // run steps with chosen amount of steps
        runSteps(getSteps());
        break;
      case 4:
        // print fractals to console
        runSteps(10000000);
        break;
      case 5:
        exit();
        break;
      default:
        System.out.println("Invalid input");
        break;
    }
    // call menu again
    mainMenu();
  }

  /**
   * The fractal menu of the Chaos Game application.
   * <p>
   *   The method prints the fractal menu options and gets user input.
   *   The method returns the path of the chosen fractal.
   * </p>
   *
   * @return the path of the chosen fractal
   */
  private String fractalMenu() {
    Scanner scanner = new Scanner(System.in);
    String path = null;
    System.out.println("Choose fractal:");
    try {
      List<String> resources = getResources();
      // If no fractals are found, return to main menu
      if (resources.isEmpty()) {
        System.out.println("No fractals found");
        return path;
      }
      // Get user input and return answer
      int ans = Utility.getNewIntRange(scanner, resources.size());
      return resources.get(ans - 1);
    } catch (IOException e) {
      System.out.println("Error: " + e.getMessage());
    }
    return path;
  }

  /**
   * Gets the resources from the fractals directory.
   * <p>
   *   The method gets the resources from the fractals directory and returns a list of the paths.
   * </p>
   *
   * @return the list of paths
   *
   * @throws IOException if an I/O error occurs
   */
  private List<String> getResources() throws IOException {
    Path resourcesDirectory = Paths.get("src/main/resources/fractals");
    List<String> pathNames = new ArrayList<>();
    try (Stream<Path> paths = Files.walk(resourcesDirectory, 1)) {
      paths
          .filter(Files::isRegularFile)
          .peek(path -> System.out.println((pathNames.size() + 1) + ". " + path.getFileName()))
          .forEach(path -> pathNames.add(path.toString()));
    }
    return pathNames;
  }

  /**
   * Reads data from file.
   * <p>
   *   The method reads data from file and creates a new ChaosGame object.
   * </p>
   *
   * @throws IOException if an I/O error occurs
   */
  private void readFromFile() throws IOException {
    System.out.println("Reading from file...");
    // Get file path from user
    filePath = fractalMenu();

    // If no file path is found, return to main menu
    if (filePath == null) {
      System.out.println("No file path found, aborting...");
      return;
    }
    // Read from file and create a new ChaosGame object
    System.out.println("File path: " + filePath);
    try {
      chaosGameDescription = fileHandler.readFromFile(filePath);
      chaosGame = new ChaosGame(chaosGameDescription, WIDTH, HEIGHT);
    } catch (IOException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Writes to file.
   * <p>
   *   The method writes the ChaosGameDescription object to file.
   * </p>
   */
  private void writeToFile() {
    System.out.println("Writing to file...");
    try {
      fileHandler.writeToFile(chaosGameDescription, filePath);
    } catch (IOException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Gets the number of steps from the user.
   * <p>
   *   The method gets the number of steps from the user and returns the number.
   * </p>
   *
   * @return the number of steps
   */
  private int getSteps() {
    System.out.println("Enter number of steps:");
    Scanner scanner = new Scanner(System.in);
    return scanner.nextInt();
  }

  /**
   * Runs steps.
   * <p>
   *   The method runs steps and prints the fractals to console.
   * </p>
   *
   * @param steps The number of steps.
   */
  private void runSteps(int steps) {
    System.out.println("Running steps...");
    if (chaosGameDescription == null || chaosGame == null) {
      System.out.println("No fractal found, aborting...");
      return;
    }
    // Create a new ChaosGame object and run steps
    chaosGame = new ChaosGame(chaosGameDescription, WIDTH, HEIGHT);
    try {
      // run steps and print fractals to console
      chaosGame.runSteps(steps);
      printFractalsToConsole();
    } catch (IllegalArgumentException e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  /**
   * Prints fractals to console.
   * <p>
   *   The method prints the fractals to console.
   * </p>
   */
  private void printFractalsToConsole() {
    System.out.println("Printing fractals to console...");
    if (chaosGameDescription == null || chaosGame == null) {
      System.out.println("No fractal found, aborting...");
      return;
    }
    for (int i = 0; i < chaosGame.getCanvas().getCanvasArray().length; i++) {
      for (int j = 0; j < chaosGame.getCanvas().getCanvasArray()[i].length; j++) {
        System.out.print((chaosGame.getCanvas().getCanvasArray()[i][j] != 1) ? "x" : " ");
      }
      System.out.println();
    }
  }

  /**
   * Exits the application.
   */
  private void exit() {
    System.out.println("Exiting...");
    System.exit(0);
  }

  /**
   * Starts the Chaos Game application.
   * <p>
   *   The method reads from file and starts the main menu.
   * </p>
   */
  public void start() {
    //Read from file
    System.out.println("Chaos Game Application");
    try {
      readFromFile();
      mainMenu();
    } catch (IOException e) {
      // If an error occurs, print the error message and start over
      System.out.println("Error: " + e.getMessage());
      exit();
    }
  }
}
