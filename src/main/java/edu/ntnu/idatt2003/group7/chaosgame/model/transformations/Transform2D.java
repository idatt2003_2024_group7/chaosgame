package edu.ntnu.idatt2003.group7.chaosgame.model.transformations;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;

/**
 * The Transform2D interface defines a contract for objects that represent 2D transformations.
 * Implementations of this interface should provide methods to transform 2D vectors or points.
 */
public interface Transform2D {

  /**
   * Transforms the given 2D vector or point according to the transformation defined by this object.
   *
   * @param point the 2D vector or point to be transformed
   * @return the transformed 2D vector or point
   */
  Vector2D transform(Vector2D point);
}
