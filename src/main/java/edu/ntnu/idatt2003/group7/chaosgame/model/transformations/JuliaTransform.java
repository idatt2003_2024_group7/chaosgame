package edu.ntnu.idatt2003.group7.chaosgame.model.transformations;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;

/**
 * The JuliaTransform class represents a 2D Julia transformation.
 * <p>
 * The class provides methods to transform a point as the Julia transformation.
 * the class implements the {@link Transform2D} interface.
 * </p>
 *
 * @version 1.0
 * @since 0.1
 * @author Ylva Björnerstedt, Helene Kjerstad
 * @see Transform2D
 */
public class JuliaTransform implements Transform2D {
  /**
   * The point c in the Julia transformation.
   */
  private final Complex point;
  /**
   * The sign in the Julia transformation.
   */
  private final int sign;

  /**
   * Constructor for JuliaTransform,
   * creates a new 2D Julia transformation with the given point and sign.
   *
   * @param point The point.
   * @param sign  The sign.
   * @throws IllegalArgumentException if: <ul>
   *                                  <li>Invalid sign, sign has to be 1 or -1</li>
   *                                  <li>Invalid point</li>
   *                                  </ul>
   */
  public JuliaTransform(Complex point, int sign) throws IllegalArgumentException {
    try {
      validateSign(sign);
      validateVector(point);
      this.point = point;
      this.sign = sign;
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Unable to create Julia transformation. "
          + e.getMessage());
    }
  }

  /**
   * Validates that the sign is a valid sign. The sign must be either 1 or -1.
   *
   * @throws IllegalArgumentException if the sign is not valid.
   */
  private void validateSign(int sign) throws IllegalArgumentException {
    if (sign != 1 && sign != -1) {
      throw new IllegalArgumentException("Sign must be 1 or -1");
    }
  }

  /**
   * Validates that the vector is a valid 2D vector.
   *
   * @throws IllegalArgumentException if the vector is not valid.
   */
  private void validateVector(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("The vector cannot be null");
    }
  }

  /**
   * Transforms the given point as the Julia transformation.
   *
   * @param pointB the point to be transformed
   * @return the transformed point
   * @throws IllegalArgumentException if the point is not valid.
   */
  @Override
  public Vector2D transform(Vector2D pointB) throws IllegalArgumentException {
    validateVector(pointB);
    Vector2D z = pointB.subtract(point);
    Complex result = new Complex(z.getX0(), z.getX1());
    return result.sqrt().scale(sign);
  }

  /**
   * Returns the string representation of the Julia transformation.
   *
   * @return the string representation of the Julia transformation
   */
  @Override
  public String toString() {
    return point.toString();
  }

  /**
   * Returns the point in the Julia transformation.
   *
   * @return the point in the Julia transformation
   */
  public Complex getPoint() {
    return point;
  }
}
