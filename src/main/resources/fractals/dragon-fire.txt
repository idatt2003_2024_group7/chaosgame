Affine2D                                # Type of transform
-300, -300                              # Lower left
300, 300                                # Upper right
.5, 0, 0, .8, 0, 50                     # 1. transform
.5, .2, -.2, .5, -100, -100             # 2. transform
.5, -.2, .2, .5, 100, -100              # 3. transform
