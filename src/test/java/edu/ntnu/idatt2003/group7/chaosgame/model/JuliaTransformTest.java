package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class JuliaTransformTest {
  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @Test
    @DisplayName("Should create a Julia transformation when valid parameters are provided")
    void shouldCreateJuliaTransformationWhenValidParametersAreProvided() {
      try {
        new JuliaTransform(new Complex(1, 2), 1);
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should transform a point when valid parameters are provided")
    void shouldTransformPointWhenValidParametersAreProvided() {
      try {
        JuliaTransform juliaTransform = new JuliaTransform(new Complex(0.3, 0.6), 1);
        Vector2D result = juliaTransform.transform(new Vector2D(0.4, 0.2));
        //Calculating the expected result
        double expectedRealPart = Math.sqrt(0.5 * (Math.sqrt(0.17) + 0.1));
        double expectedImaginaryPart = Math.sqrt(0.5 * (Math.sqrt(0.17) - 0.1));
        //Checking if the result is close to the expected result
        Assertions.assertTrue(((result.getX0() - expectedRealPart) < 1e-6)
            && ((result.getX1() - expectedImaginaryPart) < 1e-6));
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return the transformation as a string")
    void shouldReturnTransformationAsString() {
      try {
        String result = new JuliaTransform(new Complex(1, 0.5), 1).toString();
        Assertions.assertEquals("1, .5", result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }
  @Nested
  @DisplayName("Negative tests")
  class MethodsDoThrowExceptions {
    @Test
    @DisplayName("Constructor should not create a Julia transformation "
        + "when invalid sign is provided as a parameter")
    void constructorShouldNotCreateJuliaTransformationWhenInvalidSignIsProvidedAsAParameter() {
      try {
        new JuliaTransform(new Complex(1, 2), 0);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create Julia transformation. "
            + "Sign must be 1 or -1", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should not create a Julia transformation "
        + "when invalid point is provided as a parameter")
    void constructorShouldNotCreateJuliaTransformationWhenInvalidPointIsProvidedAsAParameter() {
      try {
        new JuliaTransform(null, 1);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create Julia transformation. "
            + "The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName(
        "Transform should not transform a point when invalid point is provided as a parameter")
    void transformShouldNotTransformPointWhenInvalidPointIsProvidedAsAParameter() {
      try {
        JuliaTransform juliaTransform = new JuliaTransform(new Complex(0.3, 0.6), 1);
        juliaTransform.transform(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }
  }
}
