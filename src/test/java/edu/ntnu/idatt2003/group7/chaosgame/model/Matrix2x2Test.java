package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class Matrix2x2Test {
  @Nested
  @DisplayName("Positive tests")
  class MethodsDoesNotThrowExceptions {

    @Test
    @DisplayName("Should create a matrix when valid parameters are provided")
    void shouldCreateMatrixWhenValidParametersAreProvided() {
      try {
        new Matrix2x2(1, 2, 3, 4);
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should multiply a matrix with a vector when valid parameters are provided")
    void shouldMultiplyMatrixWithVectorWhenValidParametersAreProvided() {
      try {
        Vector2D vector = new Matrix2x2(1, 2, 3, 4).multiply(new Vector2D(1, 2));
        Assertions.assertEquals(5, vector.getX0());     // 1*1 + 2*2 = 5
        Assertions.assertEquals(11, vector.getX1());    // 3*1 + 4*2 = 11
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return the matrix as a string")
    void shouldReturnMatrixAsString() {
      try {
        String result = new Matrix2x2(1, 2, 3, 0.5).toString();
        Assertions.assertEquals("1, 2, 3, .5", result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowsExceptions {

    @Test
    @DisplayName("Constructor should throw an exception when NaN is provided as a parameter")
    void constructorShouldThrowAnExceptionWhenNaNIsProvidedAsAParameter() {
      try {
        new Matrix2x2(Double.NaN, 2, 3, 4);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create Matrix2x2 object. a00 is not a valid double",
            e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Multiply should throw an exception when null is provided as a parameter")
    void multiplyShouldThrowAnExceptionWhenNullIsProvidedAsAParameter() {
      try {
        new Matrix2x2(1, 2, 3, 4).multiply(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }
  }
}
