package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ChaosGameDescriptionFactoryTest {

  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {
    @Test
    @DisplayName("Should print julia transform")
    void shouldMakeJuliaPreset() {
      ChaosGameDescription description = ChaosGameDescriptionFactory.juliaDescription();
      Assertions.assertNotNull(description);
    }

    @Test
    @DisplayName("Should print barnsley affine")
    void shouldMakeBarnsleyPreset() {
      ChaosGameDescription description = ChaosGameDescriptionFactory.barnsleyDescription();
      Assertions.assertNotNull(description);
    }

    @Test
    @DisplayName("Should print sierpinski affine")
    void shouldPrintSierpinskiPreset() {
      ChaosGameDescription description = ChaosGameDescriptionFactory.sierpinskiDescription();
      Assertions.assertNotNull(description);
    }
  }

}
