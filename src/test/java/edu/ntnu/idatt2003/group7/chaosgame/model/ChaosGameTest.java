package edu.ntnu.idatt2003.group7.chaosgame.model;

import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGame;
import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.io.ChaosGameFileHandler;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ChaosGameTest {

  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @Test
    @DisplayName("Should create a ChaosGame when valid parameters are provided")
    void shouldCreateChaosGameWhenValidParametersAreProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription =
            chaosGameFileHandler.readFromFile("src/main/resources/fractals/sierpinski.txt");
        ChaosGame chaosGame = new ChaosGame(chaosGameDescription, 100, 100);
        Assertions.assertNotNull(chaosGame);

      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should run steps when valid parameters are provided")
    void shouldRunStepsWhenValidParametersAreProvided() {
      try {
        JuliaTransform juliaTransform = new JuliaTransform(
            new Complex(-0.74543, 0.11301), 1);
        List <Transform2D> transforms = new ArrayList<>();
        transforms.add(juliaTransform);
        ChaosGameDescription description =
            new ChaosGameDescription(transforms, new Vector2D(-1.6, -1), new Vector2D(1.6, 1));
        ChaosGame chaosGame = new ChaosGame(description, 1000, 1000);
        chaosGame.runSteps(1);
        Vector2D ex = juliaTransform.transform(new Vector2D(0, 0));
        Assertions.assertEquals(ex.getX0(), chaosGame.getCurrentPoint().getX0());
        Assertions.assertEquals(ex.getX1(), chaosGame.getCurrentPoint().getX1());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should display the Sierpinski triangle")
    void shouldDisplayTheSierpinskiTriangle() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0, 0)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.5, 0)));
        ChaosGame chaosGame = new ChaosGame(new ChaosGameDescription(
            transforms,new Vector2D(0,0),new Vector2D(1,1)), 60, 60);
        chaosGame.runSteps(10000);
        for (int i = 0; i< chaosGame.getCanvas().getCanvasArray().length; i++) {
          for (int j = 0; j< chaosGame.getCanvas().getCanvasArray()[i].length; j++) {
            System.out.print((chaosGame.getCanvas().getCanvasArray()[i][j] == 1) ? "X" : " ");
          }
          System.out.println();
        }
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should update the ChaosGame when given valid parameters")
    void shouldUpdateChaosGameWhenGivenValidParameters() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0, 0)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.5, 0)));
        ChaosGame chaosGame = new ChaosGame(new ChaosGameDescription(
            transforms,new Vector2D(0,0),new Vector2D(1,1)), 60, 60);

        // Update the ChaosGame
        List<Transform2D> newTransforms = new ArrayList<>();
        newTransforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0, 0)));
        chaosGame.updateChaosGameDescription(new ChaosGameDescription(
            newTransforms, new Vector2D(0,0), new Vector2D(1,1)));

        Assertions.assertEquals(chaosGame.getDescription().getTransforms(), newTransforms);
        Assertions.assertEquals(chaosGame.getCanvas().getMaxCoords(), new Vector2D(1, 1));
        Assertions.assertEquals(chaosGame.getCanvas().getMinCoords(), new Vector2D(0, 0));
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions{

    @Test
    @DisplayName("Should not create a ChaosGame when ChaosGameDescription is null")
    void shouldNotCreateChaosGameWhenChaosGameDescriptionIsNull() {
      try {
        new ChaosGame(null, 100, 100);
        Assertions.fail("The test failed because the constructor did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create ChaosGame object. Description cannot be null",
            e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not create a ChaosGame when width is invalid")
    void shouldNotCreateChaosGameWhenWidthIsInvalid() {
      try {
        new ChaosGame(new ChaosGameDescription(
            List.of(new JuliaTransform(new Complex(1,1),1)), new Vector2D(0,0),
            new Vector2D(1, 1)), -1, 100);
        Assertions.fail("The test failed because the constructor did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals(
            "Unable to create ChaosGame object. The width must be a positive int",
            e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not create a ChaosGame when height is invalid")
    void shouldNotCreateChaosGameWhenHeightIsInvalid() {
      try {
        new ChaosGame(new ChaosGameDescription(
            List.of(new JuliaTransform(new Complex(1,1),1)), new Vector2D(0,0),
            new Vector2D(1, 1)), 100, -1);
        Assertions.fail("The test failed because the constructor did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals(
            "Unable to create ChaosGame object. The height must be a positive int",
            e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1, -30})
    @DisplayName("Should not run steps when steps is invalid")
    void shouldNotRunStepsWhenStepsIsInvalid() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription =
            chaosGameFileHandler.readFromFile("src/main/resources/fractals/sierpinski.txt");
        ChaosGame chaosGame = new ChaosGame(chaosGameDescription, 100, 100);
        Assertions.assertThrows(IllegalArgumentException.class, () -> chaosGame.runSteps(-1));
      } catch (Exception e) {
        Assertions.fail("The test failed because the method did not throw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not update the ChaosGame when given invalid parameters")
    void shouldNotUpdateChaosGameWhenGivenChaosGameDescriptionIsNull() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0, 0)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)));
        transforms.add(new AffineTransform2D(new Matrix2x2(
            0.5, 0, 0, 0.5), new Vector2D(0.5, 0)));
        ChaosGame chaosGame = new ChaosGame(new ChaosGameDescription(
            transforms,new Vector2D(0,0),new Vector2D(1,1)), 60, 60);

        // Update the ChaosGame
        Assertions.assertThrows(IllegalArgumentException.class, () ->
            chaosGame.updateChaosGameDescription(null));

      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

}
