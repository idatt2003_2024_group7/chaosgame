package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosCanvas;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ChaosCanvasTest {
  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @Test
    @DisplayName("Constructor should create a new canvas when valid parameters are provided")
    void constructorShouldCreateNewCanvasWhenValidParametersAreProvided() {
      try {
        ChaosCanvas chaosCanvas = new ChaosCanvas(
            100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        Assertions.assertNotNull(chaosCanvas);
        Assertions.assertEquals(100, chaosCanvas.getWidth());
        Assertions.assertEquals(100, chaosCanvas.getHeight());
        Assertions.assertEquals(new Vector2D(0, 0), chaosCanvas.getMinCoords());
        Assertions.assertEquals(new Vector2D(1, 1), chaosCanvas.getMaxCoords());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("setPixel should set the pixel at the given point")
    void setPixelShouldSetThePixelAtTheGivenPoint() {
      try {
        ChaosCanvas chaosCanvas = new ChaosCanvas(
            100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        Vector2D point = new Vector2D(0, 0);
        chaosCanvas.setPixel(point);
        Assertions.assertEquals(1, chaosCanvas.getPixel(point));
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("clear method should empty the canvas")
    void clearMethodShouldEmptyTheCanvas() {
      try {
        ChaosCanvas chaosCanvas = new ChaosCanvas(
            100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        Vector2D point = new Vector2D(0, 0);
        chaosCanvas.setPixel(point);
        chaosCanvas.clear();
        int [][] canvas = new int[100][100];
        Assertions.assertEquals(0, chaosCanvas.getPixel(point));
        Assertions.assertArrayEquals(canvas, chaosCanvas.getCanvasArray());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {

    @Test
    @DisplayName("Constructor should throw exception when width is not a positive int")
    void constructorShouldThrowExceptionWhenWidthIsNotAPositiveInt() {
      try {
        new ChaosCanvas(-100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "Unable to create ChaosCanvas object. The width must be a positive int",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when height is not a positive int")
    void constructorShouldThrowExceptionWhenHeightIsNotAPositiveInt() {
      try {
        new ChaosCanvas(100, -100, new Vector2D(0, 0), new Vector2D(1, 1));
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "Unable to create ChaosCanvas object. The height must be a positive int",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when minCoords is null")
    void constructorShouldThrowExceptionWhenMinCoordsIsNull() {
      try {
        new ChaosCanvas(100, 100, null, new Vector2D(1, 1));
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "Unable to create ChaosCanvas object. The vector minCoords cannot be null",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw exception when maxCoords is null")
    void constructorShouldThrowExceptionWhenMaxCoordsIsNull() {
      try {
        new ChaosCanvas(100, 100, new Vector2D(0, 0), null);
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "Unable to create ChaosCanvas object. The vector maxCoords cannot be null",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("getPixel should throw exception when point is null")
    void getPixelShouldThrowExceptionWhenPointIsNull() {
      try {
        ChaosCanvas chaosCanvas = new ChaosCanvas(
            100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        chaosCanvas.getPixel(null);
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "The vector point cannot be null",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("setPixel should throw exception when point is null")
    void setPixelShouldThrowExceptionWhenPointIsNull() {
      try {
        ChaosCanvas chaosCanvas = new ChaosCanvas(
            100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        chaosCanvas.setPixel(null);
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "The vector point cannot be null",
            e.getMessage());
      }
    }
  }
}
