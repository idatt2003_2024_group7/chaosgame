package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.io.ChaosGameFileHandler;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

class ChaosGameFileHandlerTest {
  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @AfterEach
    void tearDown() {
      try {
        Files.deleteIfExists(Path.of("src/main/resources/fractals/fractalTest.txt"));
      } catch (IOException e) {
        Assertions.fail("Unable to delete file fractalTest.txt");
      }
    }

    @Test
    @DisplayName("Should read Affine transform from file when valid file path is provided")
    void shouldReadAffineFromFileWhenValidFilePathIsProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile("src/main/resources/fractals/sierpinski.txt");
        Assertions.assertNotNull(chaosGameDescription);
        Assertions.assertEquals(new Vector2D(0, 0), chaosGameDescription.getMinCoords());
        Assertions.assertEquals(new Vector2D(1, 1), chaosGameDescription.getMaxCoords());
        Assertions.assertEquals(3, chaosGameDescription.getTransforms().size());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should read Julia transform from file when valid file path is provided")
    void shouldReadJuliaFromFileWhenValidFilePathIsProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile("src/main/resources/fractals/julia.txt");
        Assertions.assertNotNull(chaosGameDescription);
        Assertions.assertEquals(chaosGameDescription.getMinCoords(), new Vector2D(-1.6, -1));
        Assertions.assertEquals(chaosGameDescription.getMaxCoords(), new Vector2D(1.6, 1));
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should write to file when valid ChaosGameDescription and file path is provided")
    void shouldWriteToFileWhenValidChaosGameDescriptionAndFilePathIsProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription = new ChaosGameDescription(List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0))),
            new Vector2D(0, 0), new Vector2D(1, 1));
        chaosGameFileHandler.writeToFile(chaosGameDescription, "src/main/resources/fractals/fractalTest.txt");
        ChaosGameDescription chaosGameDescriptionFromFile =
            chaosGameFileHandler.readFromFile("src/main/resources/fractals/fractalTest.txt");
        // add an equals method to ChaosGameDescription ???
        Assertions.assertEquals(chaosGameDescription.getMinCoords(),
            chaosGameDescriptionFromFile.getMinCoords());
        Assertions.assertEquals(chaosGameDescription.getMaxCoords(),
            chaosGameDescriptionFromFile.getMaxCoords());
        Assertions.assertEquals(chaosGameDescription.getTransforms().size(),
            chaosGameDescriptionFromFile.getTransforms().size());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {

    @Test
    @DisplayName("readFromFile should throw IOException when invalid file path is provided")
    void readShouldThrowIOExceptionWhenInvalidFilePathIsProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        chaosGameFileHandler.readFromFile("src/main/resources/invalid.txt");
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (Exception e) {
        Assertions.assertEquals(
            "Unable to read data from src/main/resources/invalid.txt"
                + "\ndue to " + new File("src/main/resources/invalid.txt"), e.getMessage());
      }
    }

    @Test
    @DisplayName("writeToFile should throw IOException when invalid file path is provided")
    void writeShouldThrowIOExceptionWhenInvalidFilePathIsProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        chaosGameFileHandler.writeToFile(null, "src/main/resources/invalid.txt");
        Assertions.fail("The test failed because the method did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Description cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. " + e.getMessage());
      }
    }
  }
}
