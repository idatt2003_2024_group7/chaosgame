package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


class Vector2DTest {

  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {
    @Test
    @DisplayName("Should create a vector when valid parameters are provided")
    void shouldCreateVectorWhenValidParametersAreProvided() {
      try {
        new Vector2D(1, 2);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should add two vectors when valid parameters are provided")
    void shouldReturnSumAsNewVectorWhenValidParametersAreProvided() {
      try {
        Vector2D result = new Vector2D(1, 2).add(new Vector2D(3, 4));
        Assertions.assertEquals(new Vector2D(4, 6), result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }

    }

    @Test
    @DisplayName("Should subtract two vectors when valid parameters are provided")
    void shouldReturnDifferenceAsNewVectorWhenValidParametersAreProvided() {
      try {
        Vector2D result = new Vector2D(1, 2).subtract(new Vector2D(3, 4));
        Assertions.assertEquals(new Vector2D(-2, -2), result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }

    }

    @Test
    @DisplayName("Should scale the vector when a valid parameter is provided")
    void shouldReturnScaledVectorWhenValidParameterIsProvided() {
      try {
        Vector2D result = new Vector2D(1, 2).scale(2);
        Assertions.assertEquals(new Vector2D(2, 4), result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return the vector as a string")
    void shouldReturnVectorAsString() {
      try {
        String result = new Vector2D(1, -2).toString();
        Assertions.assertEquals("1, -2", result);
        result = new Vector2D(0.5, -2.5).toString();
        Assertions.assertEquals(".5, -2.5", result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {
    @Test
    @DisplayName("Constructor should trow an exception when NaN is provided as a parameter")
    void constructorShouldThrowAnExceptionWhenNaNIsProvidedAsAParameter() {
      try {
        new Vector2D(1, Double.NaN);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create vector. The value is not a valid double", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Add should throw an exception when null is provided as a parameter")
    void addShouldThrowAnExceptionWhenNullIsProvidedAsAParameter() {
      try {
        new Vector2D(1, 2).add(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an exception when Null is provided as a parameter")
    void subtractShouldThrowAnExceptionWhenNullIsProvidedAsAParameter() {
      try {
        new Vector2D(1, 2).subtract(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an exception when NaN is provided as a parameter")
    void scaleShouldThrowAnExceptionWhenNaNIsProvidedAsAParameter() {
      try {
        new Vector2D(1, 2).scale(Double.NaN);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The value is not a valid double", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }
  }
}
