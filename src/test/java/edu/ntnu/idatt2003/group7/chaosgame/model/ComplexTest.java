package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ComplexTest {

  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {
    @Test
    @DisplayName("Should create a complex vector when valid parameters are provided")
    void shouldCreateComplexVectorWhenValidParametersAreProvided() {
      try {
        new Complex(1, 2);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName(
        "Should calculate the square of the complex number when valid parameters are provided")
    void shouldCalculateSquareOfComplexNumberWhenValidParametersAreProvided() {
      try {
        Complex result = new Complex(1, 1).sqrt();
        //Calculating the expected result
        double expectedRealPart = Math.pow(2, 0.25) * Math.sqrt(Math.sqrt(2) / 4 + 0.5);
        double expectedImaginaryPart = Math.pow(2, 0.25) * Math.sqrt(-Math.sqrt(2) / 4 + 0.5);
        //Checking if the result is close to the expected result
        Assertions.assertTrue((Math.abs(result.getX0() - expectedRealPart) < 1e-6)
            && (Math.abs(result.getX1() - expectedImaginaryPart) < 1e-6));

      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {
    @Test
    @DisplayName("Constructor should throw an exception when NaN is provided as a parameter")
    void constructorShouldThrowAnExceptionWhenNaNIsProvidedAsAParameter() {
      try {
        new Complex(1, Double.NaN);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create vector. The value is not a valid double",
            e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }
  }
}
