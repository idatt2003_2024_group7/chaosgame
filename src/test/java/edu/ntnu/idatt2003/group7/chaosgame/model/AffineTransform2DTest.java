package edu.ntnu.idatt2003.group7.chaosgame.model;

import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class AffineTransform2DTest {
  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @Test
    @DisplayName("Should create an affine transformation when valid parameters are provided")
    void shouldCreateAffineTransformWhenValidParametersAreProvided() {
      try {
        new AffineTransform2D(new Matrix2x2(1, 2, 3, 4), new Vector2D(1, 2));
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should transform a point when valid parameters are provided")
    void shouldTransformPointWhenValidParametersAreProvided() {
      try {
        Vector2D result = new AffineTransform2D(new Matrix2x2(1, 2, 3, 4), new Vector2D(1, 2))
            .transform(new Vector2D(1, 2));
        Assertions.assertEquals(6, result.getX0());        // 1*1 + 2*2 + 1 = 6
        Assertions.assertEquals(13, result.getX1());       // 3*1 + 4*2 + 2 = 13
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return the transformation as a string")
    void shouldReturnTransformationAsString() {
      try {
        String result = new AffineTransform2D(new Matrix2x2(1, 2, 3, 4), new Vector2D(1, 2)).toString();
        Assertions.assertEquals("1, 2, 3, 4, 1, 2", result);
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {

    @Test
    @DisplayName("Constructor should throw an exception when matrix is null")
    void constructorShouldThrowAnExceptionWhenMatrixIsNull() {
      try {
        new AffineTransform2D(null, new Vector2D(1, 2));
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create AffineTransform2D object. The matrix cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor should throw an exception when vector is null")
    void constructorShouldThrowAnExceptionWhenVectorIsNull() {
      try {
        new AffineTransform2D(new Matrix2x2(1, 2, 3, 4), null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create AffineTransform2D object. The vector cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an unexpected exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an exception when point is null")
    void shouldThrowAnExceptionWhenPointIsNull() {
      try {
        new AffineTransform2D(new Matrix2x2(1, 2, 3, 4), new Vector2D(1, 2))
            .transform(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("The point cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an unexpected exception. "
            + e.getMessage());
      }
    }
  }
}
