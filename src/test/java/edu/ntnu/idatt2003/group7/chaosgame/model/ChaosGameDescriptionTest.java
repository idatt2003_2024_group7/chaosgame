package edu.ntnu.idatt2003.group7.chaosgame.model;

import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2003.group7.chaosgame.model.fractal.ChaosGameDescription;
import edu.ntnu.idatt2003.group7.chaosgame.model.io.ChaosGameFileHandler;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Complex;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2003.group7.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.group7.chaosgame.model.transformations.Transform2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ChaosGameDescriptionTest {

  @Nested
  @DisplayName("Positive tests")
  class MethodsDoNotThrowExceptions {

    @Test
    @DisplayName("Should create an instance when valid parameters are provided")
    void shouldCreateAnInstanceWhenValidParametersAreProvided() {
      try {
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription =
            chaosGameFileHandler.readFromFile("src/main/resources/fractals/sierpinski.txt");
        Assertions.assertEquals(new Vector2D(0, 0), chaosGameDescription.getMinCoords());
        Assertions.assertEquals(new Vector2D(1, 1), chaosGameDescription.getMaxCoords());
        Assertions.assertEquals(3, chaosGameDescription.getTransforms().size());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return a string representation of the julia fractal description")
    void shouldReturnAStringRepresentationOfTheJuliaFractalDescription() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new JuliaTransform(new Complex(0.3, 0.6), 1));
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(2, 2));
        Assertions.assertEquals(
            """
            Julia                                   # Type of transform
            0, 0                                    # Lower left
            2, 2                                    # Upper right
            .3, .6                                  # Real and imaginary parts of the constant c
            """,
            chaosGameDescription.toString());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return a string representation of the affine fractal description")
    void shouldReturnAStringRepresentationOfTheAffineFractalDescription() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new AffineTransform2D(new Matrix2x2(1, 0, -0.4, 1), new Vector2D(1, 1)));
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(2, 2));
        Assertions.assertEquals(
            """
            Affine2D                                # Type of transform
            0, 0                                    # Lower left
            2, 2                                    # Upper right
            1, 0, -.4, 1, 1, 1                      # 1. transform
            """,
            chaosGameDescription.toString());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class MethodsThrowExceptions {

    @Test
    @DisplayName("Should throw an Exception when transforms provided is null")
     void shouldNotCreateAnInstanceWhenTransformsProvidedIsNull() {
      try {
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(null, new Vector2D(0, 0), new Vector2D(2, 2));

      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create ChaosGameDescription object. "
            + "The list cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an Exception when transforms provided is empty")
    void shouldNotCreateAnInstanceWhenTransformsProvidedIsEmpty() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(transforms, new Vector2D(0, 0), new Vector2D(2, 2));

      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create ChaosGameDescription object. "
            + "The list cannot be empty", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an Exception when minCoords provided is null")
    void shouldNotCreateAnInstanceWhenMinCoordsProvidedIsNull() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new JuliaTransform(new Complex(0.3, 0.6), 1));
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(transforms, null, new Vector2D(2, 2));

      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create ChaosGameDescription object. "
            + "The minCoords cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should throw an Exception when MaxCoords provided is null")
    void shouldNotCreateAnInstanceWhenMaxCoordsProvidedIsNull() {
      try {
        List<Transform2D> transforms = new ArrayList<>();
        transforms.add(new JuliaTransform(new Complex(0.3, 0.6), 1));
        ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
        ChaosGameDescription chaosGameDescription =
            new ChaosGameDescription(transforms, new Vector2D(1, 2), null);

      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Unable to create ChaosGameDescription object."
            + " The maxCoords cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the method threw an exception. " + e.getMessage());
      }
    }
  }
}
